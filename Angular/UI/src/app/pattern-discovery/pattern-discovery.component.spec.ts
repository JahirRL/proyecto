import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatternDiscoveryComponent } from './pattern-discovery.component';

describe('PatternDiscoveryComponent', () => {
  let component: PatternDiscoveryComponent;
  let fixture: ComponentFixture<PatternDiscoveryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatternDiscoveryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatternDiscoveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
