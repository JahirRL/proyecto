import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/servicios/api.service'; 
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { from } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {MatAccordion} from '@angular/material/expansion';
import { Infoproject } from 'src/app/models/infoproject.interface';
import { Project } from 'src/app/models/project.interface';

import { ElementRef, Renderer2, ViewChild, Output, EventEmitter, Inject } from '@angular/core';
import { update } from 'plotly.js';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ApiService]
})

export class DashboardComponent implements OnInit {

  @ViewChild(MatAccordion) accordion!: MatAccordion;
  @ViewChild('divRem') divRem!: ElementRef;

  displayedColumns: string[] = [
    "piece_id",
    "PieceName",
    "CreationDate",
    "Status",
    "Operator",
    "controlNb",
    "measuredNb",
    "failedNb"
  ];
  years = [
    {year: "2018", value: "2018"},
    {year: "2019", value: "2019"},
    {year: "2020", value: "2020"},
    //{year: "2021", value: "2021"},
  ];
  months = [
    {month: "January", value: new Date().getFullYear()+"-01-" },
    {month: "February", value: new Date().getFullYear()+"-02-" },
    {month: "March", value: new Date().getFullYear()+"-03-" },
    {month: "April", value: new Date().getFullYear()+"-04-" },
    {month: "May", value: new Date().getFullYear()+"-05-" },
    {month: "June", value: new Date().getFullYear()+"-06-" },
    {month: "July", value: new Date().getFullYear()+"-07-" },
    {month: "August", value: new Date().getFullYear()+"-08-" },
    {month: "September", value: new Date().getFullYear()+"-09-" },
    {month: "October", value: new Date().getFullYear()+"-10-" },
    {month: "November", value: new Date().getFullYear()+"-11-" },
    {month: "December", value: new Date().getFullYear()+"-12-" }
  ];

  public endDate:String = (new Date().getFullYear()+"-12-31");
  public projectName:String = "CONFIDENTIAL 65720 TVA KTH";
  public startDate:String = "2018-10-01";
  public startProjectDate!: String;
  public endProjectDate!: String;
  public variable!: String;

  public isvisibleWelMCard:boolean = true;
  public isvisibleMCard:boolean = false;
  public panelOpenState:boolean = true;
  public showFiller: boolean = false;
  //public infoButton:boolean = false;
  public isDisabled!: boolean;
  
  public isVariableVisible:number = 1;
  //public projectSelected: number = 1;
  public isVisible: number = 1; //1:Yearly, 2 Monthly

  public operatorsCounts:any=[];
  public optionsProject!: any[];
  private infoProject!: any[];
  public operatorsData:any=[];
  public variablesData:any=[];
  public dataOverview:any=[];
  public periodData:any=[];
  public dataSource : any;
  public tableData:any=[];
  public optionsP!: any[];

  getDataForm = new FormGroup({
    variable: new FormControl(),
    project: new FormControl(),
    operator: new FormControl(),
    status: new FormControl(),
    d_start: new FormControl(),
    d_end: new FormControl(),
    year: new FormControl()
  })

  public graph = {
    data: [
      { x: [''], y: [0], name: '', marker: { color: "", }, type: 'bar' },
      { x: [''], y: [0], name: '', marker: { color: "", }, type: 'bar' }
    ],
    layout: { title: ''},
    config: {responsive: false}
  };

  public graph2 = {
    data: [
        { values: 0, labels: [''], type: 'pie' },
    ],
    layout: {title: ''},
    config: {responsive: false}
  };

  public graph3 = {

    data: [
      {x: [''], y: [''], type: 'scatter'},
      {x: [''],y: [''], type: 'scatter'}
    ],
    layout: {
        title: '',
        yaxis: {
          title: "",
        }
      },
    config: {responsive: false}
  };

  constructor(private api:ApiService, public dialog: MatDialog) { }

  ngOnInit(): void {

    this.api.getProjects().subscribe(data=>{
      this.optionsP=data;

      this.getDataForm = new FormGroup({
        variable: new FormControl("project"),
        project: new FormControl(this.optionsP[1].prj_name),
        operator: new FormControl("Placeholder"),
        status: new FormControl("Placeholder"),
        d_start: new FormControl(new Date().getFullYear()+"-01-01"),
        d_end: new FormControl(new Date().getFullYear()+"-12-31"),
        year: new FormControl("2020") //Revisar
      })
    });

  }

  onSubmit(form: any) {
    this.projectName = form.project;
    this.variable = form.variable;

    if(this.isVisible == 1){
      this.startDate = form.year + '-01-01';
      this.endDate = form.year + '-12-31';
    }
    else{
      this.startDate = form.d_start;
      this.endDate = form.d_end;
    }

    this.optionsP.forEach(element => {
      if (element.prj_name == form.project) {
        this.startProjectDate = element.b_min.toString()
        this.endProjectDate = element.c_sum.toString()
      }
    });

    this.displayReport();
  }

  displayReport() {

    if(this.isvisibleWelMCard == true){
      this.closeWelcomM()
    }

    this.isvisibleMCard = true;

    switch (this.variable) {
      case "project":
        
        this.api.getDataOverview(this.startDate,this.endDate,this.projectName,"1").subscribe(data=>{
          this.dataOverview=data;
          this.tableData=data.sampleData;
          this.infoProject = data;
        });
    
        this.api.getOperators(this.startDate,this.endDate,this.projectName,"2","Operator").subscribe(data=>{
          this.operatorsData=data["categories"];
          this.pieChart();
        });
        
        this.api.getOperatorsCounts(this.startDate,this.endDate,this.projectName,"7").subscribe(data=>{
          this.operatorsCounts=data;
          this.histogram();
        });
        
        this.api.getPeriod("6","prj_name",this.startDate,this.endDate,"",this.projectName).subscribe(data=>{
          this.periodData=data;
          this.lineChart();
        });
        
        this.api.getVariablesComparison(this.startDate,this.endDate,this.projectName,"4","Status,Operator").subscribe(data=>{
          this.variablesData=data;
        });
        
        break;
        
      case "operator":

      this.histogram()
      this.pieChart()
      this.lineChart()
        

        
        break;
          
      case "status":
        this.histogram()
        this.pieChart()
        this.lineChart()
        
        break;
    
      default:
        break;
    }


  }
  
  histogram(){

    this.graph = {
      data: [
        { x: this.operatorsCounts["Operator"], y: this.operatorsCounts["Approved"], name: 'Approved',
        marker: {
          color: "rgb(144, 238, 144)",
        }, type: 'bar' },
        
        { x: this.operatorsCounts["Operator"], y: this.operatorsCounts["Rejected"], name: 'Denied',
        marker: {
          color: "rgb(255, 0, 0)",
        },
        type: 'bar' }
      ],
      layout: { title: 'Approved / Denied'},
      config: {responsive: false}
    };
  }

  pieChart() {
    
    this.graph2 = {
      
      data: [
        { values: this.operatorsData.counts, labels: this.operatorsData.Operator, type: 'pie' },
      ],
      layout: {title: 'Pieces measured by operator'},
      config: {responsive: false}
    };

  }

  lineChart(){

    this.graph3 = {
      data: [
        {x: this.periodData.index, y: this.periodData.serie,  type: 'scatter'}
      ],
      layout: {
          title: 'Controls measured per day',
          yaxis: {
            title: "Total count",
          }
        },
      config: {responsive: false}
    };
  }

  /* Events */
  onItemChange(i: any) {
   this.isVisible = i;
  }

  select(option: any){

    switch (option) {
      case "project":
        this.isVariableVisible=1;
        break;

      case "operator":
        this.isVariableVisible=2;
        break;

      case "status":
        this.isVariableVisible=3;
        break;
    
      default:
        console.log("Error")
        break;
    }

    this.isvisibleMCard = false;
    
  }

  /* Close  card and widget */
  closeWelcomM(){
    //Erase the text of the DOM
    this.divRem.nativeElement.remove();
    this.isvisibleWelMCard = false;
  }

  closeReportWdgt(){
    this.isvisibleMCard = false;
  }

}