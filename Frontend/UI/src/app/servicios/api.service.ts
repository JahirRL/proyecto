import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http"
import { Observable } from 'rxjs';
import { Project } from '../models/project.interface';
import { Timeseries } from '../models/timeseries.interface';
import { ClusterData } from '../models/clusterdata.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:string = "http://192.168.50.75:81/";
  //url:string = "http://127.0.0.1:8000/";

  constructor(private http:HttpClient) { }
  
  getCollection():Observable<any>{
    let direccion = this.url + "getCollection/";
    return this.http.get<any>(direccion);
    //return this.http.get<any[]>(this.url+'getCollection/');
  }

  getDataOverview(d_start: String, d_end:String, project:String, type:String):Observable<any>{
    let direccion = this.url + "dataOverview/?d_start="+ d_start +"&d_end="+ d_end+"&project="+ project+"&type="+ type;
    return this.http.get<any>(direccion);
  }

  getProjects():Observable<Project[]>{
    let direccion = this.url + "dataOverview/?type=3";
    return this.http.get<Project[]>(direccion);
  }

  getClusterC(project:String):Observable<ClusterData>{
    let direccion = this.url + "patternDiscovery/?&model=1&project="+project;
    return this.http.get<ClusterData>(direccion);
  }

  getTSeriesC(project:String):Observable<Timeseries>{
    let direccion = this.url + "patternDiscovery/?&model=2&project="+project;
    return this.http.get<Timeseries>(direccion);
  }

  getHeatmapC(project:String){
    let direccion = this.url + "heatMap/?project="+project;
    return this.http.get<any>(direccion);
  }

  getVariablesComparison(d_start: String, d_end:String, project:String, type:String, variable:String){
    let direccion = this.url + "dataOverview/?d_start="+ d_start +"&d_end="+ d_end+"&project="+ project+"&type="+ type+"&variable="+ variable;
    return this.http.get<any>(direccion);
  }

  getOperators(d_start: String, d_end:String, project:String, type:String, variable:String){
    let direccion = this.url + "dataOverview/?d_start="+ d_start +"&d_end="+ d_end+"&project="+ project+"&type="+ type+"&variable="+ variable;
    return this.http.get<any>(direccion);
  }
  
  getPeriod(type:String, variable:String, d_start: String, d_end:String, project:String, value:String){
    let direccion = this.url + "dataOverview/?type="+ type+"&variable="+ variable+"&d_start="+ d_start +"&d_end="+ d_end+"&project="+ project +"&value=" +value;
    return this.http.get<any>(direccion);
  }
  
  getOperatorsCounts(d_start: String, d_end:String, project:String, type:String){
    let direccion = this.url + "dataOverview/?d_start="+ d_start +"&d_end="+ d_end+"&project="+ project+"&type="+ type;
    return this.http.get<any>(direccion);
  }
}
