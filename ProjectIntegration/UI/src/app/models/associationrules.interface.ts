import { MAssociationRules } from "./modelassociationrules.interface";

export interface AssociationRules {
    variables: number, 
    observations: number,
    d_min: string,
    d_max: string,
    natural_rules: Array<string>,
    model: Array<MAssociationRules>,
}