# Data Mining PolyWorks API REST

##### New features (Friday, October 1, 2021):
- Generalized end points, now there is only one endpoint to get the descriptive information from the data overview panel.
- Implement singleton for make queries to mongodb.
- Query optimization, server response time was improved from an average of 230ms to an average of 22ms (in query pieces perspective), and from 230 ms to 100 ms (in query controls perspective).
- Code optimization, redundant code deleted.
- Database query is now based on measurement controls and not parts.

## Requirements
1. Python > 3.8
2. Virtualenv or pipenv or conda
3. Django > 3.2.6
4. PyMongo > 3.12.0
5. django-cors-headers > 3.8.0
5. MongoDB > 5.0

## Instructions
- [ ] Activate virtual environment
- [ ] Install requirements
- [ ] Adjust Cors
- [ ] run: python manage.py rumserver

## Endpoints
**GET** *host:port*/dataOverview/{name_project}/{d_start}/{d_end}/{var}/{type}

It obtains information for the data overview panel, the "type" parameter indicates whether the information will be obtained: 
- 1 for the entire dataset (before getInfoProject) 
- 2 to obtain information on an existing variable in the database (before getInfoVar)
- 3 to get a list of projects in the database

Parameters detail:

| Variable | Detail |
|:--|:--|
| name_project | Text string with the project to be selected in the query |
| d_start | Datetime of the start date range |
| d_end | Datetime of the end date range |
| var | Name of the variable to describe (in the case type is 2) |


