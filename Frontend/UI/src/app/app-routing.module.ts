import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CanDeactivateGuardService } from './servicios/can-deactivate-guard.service';
import { DashboardComponent } from './vistas/dashboard/dashboard.component';
import { DashboardpatternComponent } from './vistas/dashboardpattern/dashboardpattern.component';
import { PageNotFoundComponent } from './vistas/page-not-found/page-not-found.component';

const routes: Routes = [
  { path:'', redirectTo:'dataoverview', pathMatch:'full'},
  { path:'dataoverview', component:DashboardComponent, },
  { path:'patterndiscovery', component:DashboardpatternComponent, },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
