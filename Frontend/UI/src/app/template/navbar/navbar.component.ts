import { Component, OnInit } from '@angular/core';
import { Location,LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { ProviderAst } from '@angular/compiler';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]
})
export class NavbarComponent implements OnInit {

  location!: Location;
  private popState: boolean = false;
  private urlHistory: any[] = [];

  constructor(location: Location, router: Router) { 

    
    this.location  = location;

    location.subscribe(event => {
      let fwd = false;
      if (this.urlHistory.lastIndexOf(event.url)) {
          this.urlHistory.push(event.url);
          
          let customUrl = this.location.path().split('/')
          let customUrlF = customUrl[1].split('?')
      
          if (customUrlF[0] == 'patterndiscovery') {
            this.clickEvent2()
          }else{
            this.clickEvent1()
          }

      }
      else {
          fwd = true;
          this.urlHistory.pop();
          
          let customUrl = this.location.path().split('/')
          let customUrlF = customUrl[1].split('?')
      
          if (customUrlF[0] == 'patterndiscovery') {
            this.clickEvent2()
          }else{
            this.clickEvent1()
          }

      }
  })

  

  }

  status: boolean = true;
  status2: boolean = false;

  clickEvent1(){
    this.status = true;
    this.status2 = false;     
  }

  clickEvent2(){
    this.status = false;
    this.status2 = true; 
  }

  ngOnInit(): void {

    let customUrl = this.location.path().split('/')
    //console.log(this.location.path())
    let customUrlF = customUrl[1].split('?')

    if (customUrlF[0] == 'patterndiscovery') {
      this.clickEvent2()
    }

  }

}
