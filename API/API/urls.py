from django.urls import path
from .views import MyCollectionView, describeDataset, exampleEp, getModelResult

urlpatterns = [
    # path('getCollection/', MyCollectionView.as_view()),
    path('dataOverview/', describeDataset, name='describeDataset'),
    path('patternDiscovery/', getModelResult, name='getModelResult'),
    path('test/', exampleEp, name='exampleEp')
]
