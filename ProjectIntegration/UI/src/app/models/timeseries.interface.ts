export interface Timeseries {
    index: Array<string>,
    serie: Array<number>,
    trend: Array<number>,
    seasonality: Array<number>,
    Stationarity: boolean, 
    nl_conclusion: string
}