export interface AssociationRules {
    operator: string, 
    hours: string, 
    status: string,
}