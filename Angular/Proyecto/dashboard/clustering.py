import pandas as pd
import numpy as np
import os

def clustering():

    data = pd.read_csv(os.getcwd()+r"\dashboard\data.csv") # lecture of data
    rdata=data.drop(['Model','Number of Doors','Market Category','Transmission Type','Driven_Wheels','highway MPG'],axis=1)
    df=rdata.dropna()

    x1=rdata.loc[:,["MSRP","city mpg"]].values

    from sklearn.cluster import KMeans
    wcss = []

    for k in range(1,11):
        kmeans=KMeans(n_clusters=k, init='k-means++')
        kmeans.fit(x1)
        wcss.append(kmeans.inertia_)

    kmeans = KMeans(n_clusters=3)
    label = kmeans.fit_predict(x1)

    return wcss, x1, kmeans

