import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class CanDeactivateGuardService {

  public stringPr = "0"

  getData(){
    return this.stringPr;
  }
  
  changeData(value: string){
    this.stringPr = value;
  }

}
