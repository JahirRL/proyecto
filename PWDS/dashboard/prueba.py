'''import pandas as pd

data = pd.read_csv("data.csv").to_dict('split')

print(data['data'][0])

tmp = data['data'][0]

print(tmp[2])

tmp = data["Engine Cylinders"].values

print(tmp)

tmpd = data["Engine Cylinders"].value_counts()

print(tmpd[0])'''

import datetime
import pandas as pd
from . import analysis as an
import importlib
import numpy as np
import json
import os

def myconverter(obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, datetime.datetime):
            return obj.__str__()
        
def dumpVar():
    print(os.getcwd())
    importlib.reload(an)
    df_example = pd.read_csv(os.getcwd()+r"\dashboard\data.csv")  # lecture of data
    obj = an.Dm(df_example) # instance an object
    #general_ds = obj.dataset_statistics()  # get general info of dataset
    var_descrip = obj.dataset_variables()  # get description of all variables
    dataset = json.dumps(var_descrip["Engine Cylinders"], default=myconverter)
    return dataset

def dumpVardos():
    importlib.reload(an)
    df_example = pd.read_csv(os.getcwd()+r"\dashboard\data.csv")  # lecture of data
    obj = an.Dm(df_example) # instance an object
    #general_ds = obj.dataset_statistics()  # get general info of dataset
    dataset = obj.getAsociationRules()
    return dataset #dataset

'''importlib.reload(an)
df_example = pd.read_csv(r"data.csv")  # lecture of data
obj = an.Dm(df_example) # instance an object
general_ds = obj.dataset_statistics()  # get general info of dataset
var_descrip = obj.dataset_variables()  # get description of all variables
dataset = json.dumps(var_descrip["Engine Cylinders"], default=myconverter)
#dataset = json.dumps(int(var_descrip["Engine Cylinders"]))
print(dataset)'''

'''for i in range(len(var_descrip)):
    json_data = {
        'Make': var_descrip["Make"]
    }

print(json_data)'''
#print(dataset)
#print(var_descrip["Make"]) <---- {'uniques': 48, '% uniques': 0.40288735940909853, 'missing': 0, '% missing': 0.0, 'categories': 48}
#print(var_descrip["Model"]) <--- {'uniques': 915, '% uniques': 7.680040288735941, 'missing': 0, '% missing': 0.0, 'categories': 915}
#print(var_descrip["Year"])  <--- {'count': 11914.0, 'mean': 2010.384337753903, 'std': 7.579739887595705, 'min': 1990.0, '25%': 2007.0, '50%': 2015.0, '75%': 2016.0, 'max': 2017.0, 'uniques': 28, '% uniques': 0.23501762632197415, 'missing': 0, '% missing': 0.0, 'values': [2011, 2011, 2011, 2011, 2011, 2012, 2012, 2012, 2012, 2013, 2013, 2013, 2013, 2013, 2013, 2013, 2013, 1992, 1992, 1992, 1992, 1992, 1993, 1993, 1993, 1993, 1993,
#print(var_descrip["Engine Fuel Type"]) <--- {'uniques': 10, '% uniques': 0.08393486654356219, 'missing': 3, '% missing': 0.02518045996306866, 'categories': 10}
#print(var_descrip["Engine Fuel Type"]["categories"])

#print(var_descrip["Engine Cylinders"]["uniques"])

   # return var_descrip