import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './template/navbar/navbar.component';
import { FooterComponent } from './template/footer/footer.component';
import { DashboardComponent } from './vistas/dashboard/dashboard.component';
import { DashboardpatternComponent } from './vistas/dashboardpattern/dashboardpattern.component';

import { HttpClientModule } from '@angular/common/http';
//import { PanelComponent } from './vistas/panel/panel.component';
import { ApiService } from './servicios/api.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from 'src/app/material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './vistas/page-not-found/page-not-found.component';
//import { ContentpatternComponent } from './vistas/contentpattern/contentpattern.component';

import { CommonModule } from '@angular/common';
import * as PlotlyJS from 'plotly.js-dist-min';
import { PlotlyModule } from 'angular-plotly.js';

PlotlyModule.plotlyjs = PlotlyJS;

import { CanDeactivateGuardService } from './servicios/can-deactivate-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    DashboardComponent,
    NavbarComponent,
    DashboardpatternComponent,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    PlotlyModule
  ],
  providers: [
    ApiService,
    CanDeactivateGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
