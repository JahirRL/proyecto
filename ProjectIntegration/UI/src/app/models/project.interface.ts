export interface Project {
    prj_name: string, 
    b_min: Date, 
    b_max: Date,
}
