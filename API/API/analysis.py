# Pyton data analysis library
import pandas as pd


from pandas.api.types import is_numeric_dtype
from pandas.api.types import is_categorical_dtype
from pandas.api.types import is_object_dtype
from pandas.api.types import is_datetime64_dtype
from pandas.api.types import is_datetime64_ns_dtype

from mlxtend.frequent_patterns import association_rules, apriori

import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller

from kmodes.kprototypes import KPrototypes
import pickle

def describeNumerical(s):
    """
    describes a categorical variable in the form of series
    :param s: Series of variable
    :return: dictionary with descriptive elements of the variable
    """
    disctint = int(s.nunique())
    percent_unique = float((disctint * 100) / s.size)
    missing = int(s.isnull().sum())
    percent_missing = float((missing * 100) / s.size)
    statistics = s.describe().to_dict()
    statistics["uniques"] = disctint
    statistics["% uniques"] = percent_unique
    statistics["missing"] = missing
    statistics["% missing"] = percent_missing
    statistics["values"] = s.to_list()
    return statistics


def describeCategorical(s):
    """
    describes a categorical variable in the form of series
    :param s: Series of variable
    :return: dictionary with descriptive elements of the variable
    """
    disctint = int(s.nunique())
    percent_unique = float((disctint * 100) / s.size)
    missing = int(s.isnull().sum())
    percent_missing = float((missing * 100) / s.size)
    values = s.value_counts().reset_index().rename(columns={'index': s.name, s.name: 'counts'}).to_dict(orient='list')
    return {
        "uniques": disctint,
        "% uniques": percent_unique,
        "missing": missing,
        "% missing": percent_missing,
        "categories": values
    }

def describeDatetime(s):
    """
    describes a datetime64[ns] variable in the form of series
    :param s: Series of variable
    :return: dictionary with descriptive elements of the variable
    """
    # first we need to get mont and year and day
    dates = s.dt.date
    # after we need to get the min date and max date
    min_date = dates.min()
    max_date = dates.max()
    num_days = (max_date - min_date).days
    values = dates.value_counts().reset_index().rename(columns={'index': s.name, s.name: 'counts'}).\
        sort_values(by=s.name).to_dict(orient='list')
    return {
        "date start": min_date,
        "date end": max_date,
        "num days": num_days,
        "categories": values
    }


class Stationarity:
    def __init__(self,  significance=0.05):  ## la hipotesis nula es que no son estacionarios, la hipótesis alternativa es que si es estacionaria
        self.significance_level = significance
        self.pvalue = None
        self.is_stationarity = None

    def ADF(self, values):  # augmented dicky fuller
        result = adfuller(values, autolag='AIC')  #
        format_results = pd.Series(result[:4], index=['Statistic', 'P-value', 'Num of Lags', 'Observations'])
        for k, v in result[4].items():
            format_results[f'Crical Value {k}'] = v
        return format_results

class Dm:
    def __init__(self, my_df):
        self.df = my_df

    def datasetStatistics(self):
        """
         calculates and returns the statistics of a dataframe in a dictionary
        """
        number_var = len(self.df.columns)
        number_obs = len(self.df)
        number_cells = self.df.size
        atributes_missing = self.df.isnull().sum()
        number_missing = atributes_missing.sum()
        percent_missing = (number_missing * 100) / number_cells
        sample = self.df.sample(n=6, random_state=16).astype('object').iloc[:, 1:].to_dict(orient='records')

        return {'variables': int(number_var),
                'observations': int(number_obs),
                'cells': int(number_cells),
                'missing': int(number_missing),
                'missing per atribute': atributes_missing.to_dict(),
                '% missing': float(percent_missing),
                'sample data': sample}

    def datasetVariables(self, parse_objects=True):
        """
        calculates descriptions of all variables in the data set and returns a dictionary.
        input parameters:
            parse_objects - indicates whether object-type attributes will be converted to categories.
        """
        if parse_objects:
            columns_obj = list(self.df.select_dtypes(['object']).columns)  # get columns object-type
            for column in columns_obj:
                self.df[column] = self.df[column].astype('category')  # convert object to category
        # start atribute analysis
        d = {}
        for column in self.df.columns:
            if is_categorical_dtype(self.df[column]):
                d[column] = describeCategorical(self.df[column])
                # WARNING THIS FAILS IF ARE TWO OR MORE COLUMNS WITH THE SAME NAME
            elif is_numeric_dtype(self.df[column]):
                d[column] = describeNumerical(self.df[column])
                # WARNING THIS FAILS IF ARE TWO OR MORE COLUMNS WITH THE SAME NAME
        """
        Warning: the use of dictionaries can be costly in memory, this can be improved by using NumPy compound arrays
        and directly choosing the length of the data types, in this way if you want to store an array of integers with 
        4 digits you would only use an int8 and not an int64 (by default).
        """
        return d

    def describeVar(self, var_name):
        result = {"status": "FAIL", "message": "Se intentó procesar un tipo de dato no reconocido"}
        if var_name in self.df.columns:
            var_type = self.df.dtypes[var_name]
            print(var_type)
            if is_object_dtype(var_type):
                try:
                    self.df[var_name] = self.df[var_name].astype('category')  # convert object to category
                    result = describeCategorical(self.df[var_name])
                except BaseException:
                    result["message"] = "Se intentó procesar una variable no categórica com categórica"
            elif is_numeric_dtype(var_type):
                result = describeNumerical(self.df[var_name])
            elif is_datetime64_ns_dtype(var_type) or is_datetime64_dtype(var_type):
                result = describeDatetime(pd.to_datetime(self.df[var_name], "%Y-%m-%dT%H:%M:%S"))
        else:
            result = {"status": "FAIL", "message": "La variable indicada no existe en la base de datos"}
        return result

    def getAsociationRules(self):
        """
        BETA VERSION static asociation rules
        generate list of asociation rules in the dataset
        param min_support: f/#transacctions
        param variables:  list of variables to get asociation rules
        param number_rules: number of rules to return
        :return: dictionary with asociation rules
        """

        df = self.df[["Engine Cylinders", "Driven_Wheels", "Vehicle Size", "Number of Doors", "MSRP"]]
        df = df.dropna()
        df["Engine Cylinders"] = pd.cut(df["Engine Cylinders"], 3, labels=["Bajo", "Medio", "Alto"])
        df["Number of Doors"] = df["Number of Doors"].astype('category')
        df["Number of Doors"] = df["Number of Doors"].replace({2.0: "two", 3.0: "three", 4.0: "four"})
        df = df[df["MSRP"] < 100000]
        df["MSRP"] = pd.cut(df["MSRP"], 3, labels=["Bajo", "Medio", "Alto"])
        df = pd.get_dummies(df)
        ap = apriori(df, min_support=0.01, max_len=2, use_colnames=True)
        rules = association_rules(ap, min_threshold=0.01)
        final = rules.sort_values("lift", ascending=False).head(10).reset_index()
        final["antecedents"] = final["antecedents"].apply(lambda x: list(x)[0]).astype("unicode")
        final["consequents"] = final["consequents"].apply(lambda x: list(x)[0]).astype("unicode")
        return final.to_json(orient="index")

    def timeSeriesAnalysis(self, prj_name):
        ts = self.df.copy()
        # filter project
        ts = ts[ts["prj_name"] == prj_name]
        # cast de string a datetime64[ns]
        ts.CreationDate = pd.to_datetime(ts.CreationDate)
        # Creación de la serie de tiempo
        ts.set_index('CreationDate', inplace=True)
        # eliminación de todas las columnas menos status
        ts = ts["status"]
        # Resampleo de la serie de tiempo por día con una agregación de porcentaje en status
        ts_rs = ts.resample('D').agg(lambda x: (x == 'Fail').mean())
        # Test de dickey fuller para comprobar la estacionaridad
        adf_test = Stationarity()
        result = adf_test.ADF(ts_rs.fillna(ts_rs.mean()).values).round(6)
        estacionaria = True if result["Statistic"] < result["Crical Value 1%"] else False
        # se realiza la descomposición y se obtienen las componentes de la serie.
        decomposition = sm.tsa.seasonal_decompose(ts_rs.dropna(), model='additive', period=50)
        return {
            "index": ts_rs.dropna().index.strftime("%d-%m-%Y").tolist(),
            "serie": ts_rs.dropna().astype('object').tolist(),
            "trend": decomposition.trend.fillna(0).astype('object').tolist(),
            "seasonality": decomposition.seasonal.astype('object').tolist(),
            "stationarity": estacionaria,
            "nl_conclusion": "The percentages of failed controls per day suggest the behavior of a stationary phenomenon with 90% confidence. There is no clear reason for increase or decrease, the seasonality of the observations does not have a sequence, this means that, in general, there is no pattern of behavior in the percentages of failed controls over time with a daily frequency."
        }

    def kprototypesclustering(self, prj_name):
        """
        BETA VERSION static kprototypes clustering results
        loads the results from a previously trained model
        :return: x, y, z fixed columns as lists
                 clusters for each x,y,z point as a list
                 The x,y,z coordinates of the centroids
        """
        dt = self.df.dropna().copy()
        if prj_name == 'CONFIDENTIAL R60810 TVA_TWA Frt Side Frame':
            filename = 'clusters_R60810.pkl'
            infile = open(filename, 'rb')
            clusters = pickle.load(infile)
            infile.close()
            filename = 'centroides_R60810.pkl'
            infile = open(filename, 'rb')
            centroides = pickle.load(infile)
            infile.close()
            centroid = centroides.tolist()
            return {"x": dt['CreationDate'].tolist(),
                    "y": dt['Operator'].tolist(),
                    "z": dt['failedNb'].tolist(),
                    "clusters": clusters,
                    "centroid1": [centroid[0][6], centroid[0][8], centroid[0][0]],
                    "centroid2": [centroid[1][6], centroid[1][8], centroid[1][0]],
                    "centroid3": [centroid[2][6], centroid[2][8], centroid[2][0]],
                    "centroid4": [centroid[3][6], centroid[3][8], centroid[3][0]]}

        elif prj_name == 'CONFIDENTIAL 65720 TVA KTH':
            filename = 'clusters_65720.pkl'
            infile = open(filename, 'rb')
            clusters = pickle.load(infile)
            infile.close()
            filename = 'centroides_65720.pkl'
            infile = open(filename, 'rb')
            centroides = pickle.load(infile)
            infile.close()
            centroid = centroides.tolist()
            return {"x": dt['CreationDate'].tolist(),
                    "y": dt['Operator'].tolist(),
                    "z": dt['failedNb'].tolist(),
                    "clusters": clusters,
                    "centroid1": [centroid[0][6], centroid[0][8], centroid[0][0]],
                    "centroid2": [centroid[1][6], centroid[1][8], centroid[1][0]],
                    "centroid3": [centroid[2][6], centroid[2][8], centroid[2][0]],
                    "centroid4": [centroid[3][6], centroid[3][8], centroid[3][0]]}
