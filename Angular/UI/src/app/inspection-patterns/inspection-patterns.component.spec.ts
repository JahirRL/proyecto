import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionPatternsComponent } from './inspection-patterns.component';

describe('InspectionPatternsComponent', () => {
  let component: InspectionPatternsComponent;
  let fixture: ComponentFixture<InspectionPatternsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InspectionPatternsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionPatternsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
