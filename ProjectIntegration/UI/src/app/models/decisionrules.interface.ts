export interface DecisionRules {
    variables: number,
    observations: number,
    d_min: string,
    d_max: string,
    feature_importance: Array<string>
}