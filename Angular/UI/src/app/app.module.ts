import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PatternDiscoveryComponent } from './pattern-discovery/pattern-discovery.component';
import { InspectionPatternsComponent } from './inspection-patterns/inspection-patterns.component';

@NgModule({
  declarations: [
    AppComponent,
    PatternDiscoveryComponent,
    InspectionPatternsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
