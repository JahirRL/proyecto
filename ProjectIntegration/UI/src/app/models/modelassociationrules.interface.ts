export interface MAssociationRules {
    antecedents: string,
    consequents: string,
    confidence: number,
    support: number,
}