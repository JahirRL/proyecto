import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http"
import { Observable } from 'rxjs';
import { Project } from '../models/project.interface';
import { Timeseries } from '../models/timeseries.interface';
import { ClusterData } from '../models/clusterdata.interface';
import { AssociationRules } from '../models/associationrules.interface';
import { DecisionRules } from '../models/decisionrules.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:string = "http://192.168.50.75:81/";
  //url:string = "http://127.0.0.1:8000/";

  constructor(private http:HttpClient) { }
  
  getCollection():Observable<any>{
    let direccion = this.url + "getCollection/";
    return this.http.get<any>(direccion);
    //return this.http.get<any[]>(this.url+'getCollection/');
  }
  //-------------------------------------------Data overview------------------------------------------//

  getProjects(type:String, filter: String):Observable<any>{
    let direccion = this.url + "dataOverview/?type="+ type +"&filter="+ filter;
    return this.http.get<any>(direccion);
  }

  getDataOverview(type:String, variable: String, value:String, d_start: String, d_end:String):Observable<any>{
    let direccion = this.url + "dataOverview/?type="+ type +"&variable="+ variable +"&value="+ value +"&d_start="+ d_start +"&d_end="+ d_end;
    return this.http.get<any>(direccion);
  }

  getSecondEndpoint(type:String, variable: String, value:String, d_start: String, d_end:String, column:String):Observable<any>{
    let direccion = this.url + "dataOverview/?type="+ type +"&variable="+ variable +"&value="+ value +"&d_start="+ d_start +"&d_end="+ d_end +"&column="+ column;
    return this.http.get<any>(direccion);
  }

  getTimeSeries(type:String, variable: String, value:String, d_start: String, d_end:String){
    let direccion = this.url + "dataOverview/?type="+ type +"&variable="+ variable +"&value="+ value +"&d_start="+ d_start +"&d_end="+ d_end;
    return this.http.get<any>(direccion);
  }

  getOperatorsCounts(type:String, variable: String, value:String, d_start: String, d_end:String){
    let direccion = this.url + "dataOverview/?type="+ type +"&variable="+ variable +"&value="+ value +"&d_start="+ d_start +"&d_end="+ d_end;
    return this.http.get<any>(direccion);
  }
  
  //--------------------------------------------------------------------------------------------------//
  
  /*-----------------------------------------Pattern discovery----------------------------------------*/
  getprojectsPtD():Observable<Project[]>{
    let direccion = this.url + "dataOverview/?type=3&filter=prj_name";
    return this.http.get<Project[]>(direccion);
  }

  getClusterC(project:String):Observable<any>{
    let direccion = this.url + "patternDiscovery/?&model=1&project="+project;
    return this.http.get<any>(direccion);
  }

  getTSeriesC(project:String):Observable<Timeseries>{
    let direccion = this.url + "patternDiscovery/?&model=2&project="+project;
    return this.http.get<Timeseries>(direccion);
  }

  getHeatmapC(project:String){
    let direccion = this.url + "heatMap/?project="+project;
    return this.http.get<any>(direccion);
  }

  getAssociationR(project:String):Observable<AssociationRules>{
    let direccion = this.url + "patternDiscovery/?model=3&project="+project;
    return this.http.get<AssociationRules>(direccion);
  }

  getDecisionR():Observable<DecisionRules>{
    let direccion = this.url + "patternDiscovery/?model=4&project";
    return this.http.get<DecisionRules>(direccion);
  }

  /*--------------------------------------------------------------------------------------------------*/

}
