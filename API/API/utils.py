import json
from bson import ObjectId
from datetime import datetime


class Util:
    @staticmethod
    def getCursorAsList(cursor):
        """
        Iterate a cursor getting each element as dictionary.
        :param cursor: Pymongo Cursor
        :return: List of dictionaries, one by document in mongodb
        """
        lista_elementos = []
        if not cursor:
            print("Error, El cursor no contiene elementos")
        else:
            for i in cursor:
                lista_elementos.append(i)
        return lista_elementos

    @staticmethod
    def parseRangeStrToDatetime(str_start, str_end):
        start = str_start.split("-")
        end = str_end.split("-")
        start = datetime(int(start[0]), int(start[1]), int(start[2]))
        end = datetime(int(end[0]), int(end[1]), int(end[2]))
        return start, end

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)
