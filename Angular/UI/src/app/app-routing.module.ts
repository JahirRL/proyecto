import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InspectionPatternsComponent } from './inspection-patterns/inspection-patterns.component';
import { PatternDiscoveryComponent } from './pattern-discovery/pattern-discovery.component';

const routes: Routes = [
  {path:'inspection-patterns',component:InspectionPatternsComponent},
  {path:'pattern-discovery',component:PatternDiscoveryComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
