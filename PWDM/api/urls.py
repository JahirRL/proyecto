from django.urls import path
from .views import MyCollectionView, describeDataset, exampleEp, getModelResult, getHeatMap

urlpatterns = [
    # path('getCollection/', MyCollectionView.as_view()),
    path('dataOverview/', describeDataset, name='describeDataset'),
    path('patternDiscovery/', getModelResult, name='getModelResult'),
    path('heatMap/', getHeatMap, name='getHeatMap'),
    path('test/', exampleEp, name='exampleEp')
]
