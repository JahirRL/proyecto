from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from . import prueba as pr
from . import clustering as ct

# Create your views here.
def index(request):

    dataWCSS, x1, kmeans = ct.clustering()

    context = {
       'dataWCSS': dataWCSS,
       'xp1': x1[:,0].tolist(),
       'xp2': x1[:,1].tolist(),
       'c': kmeans.labels_.tolist(),
       'kmeansx': kmeans.cluster_centers_[:,0].tolist(),
       'kmeansy': kmeans.cluster_centers_[:,1].tolist(),
    }

    return render(request,'main.html', context)

def histogram(request):
   formatedDataOne = pr.dumpVar()
   formatedData = pr.dumpVardos()
   context = {
        'formatedDataOne': formatedDataOne,
        'formatedData': formatedData
    }
   return render(request,'histogram.html',context)