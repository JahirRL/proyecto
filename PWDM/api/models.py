from pymongo import MongoClient
from .utils import Util
from .analysis import *
import pandas as pd
import numpy as np

"""import pprint
import json
"""

# function to get accuracy of piece
def percent_deviation_control(x):
    ret = 0
    if x["deviation_value"]>0 and x["upper_tol"]!=0:
        ret = (x["deviation_value"]*100) / x["upper_tol"]
    elif x["deviation_value"]>0 and x["upper_tol"]==0:
        ret = np.nan
    if x["deviation_value"]<0 and x["lower_tol"]!=0:
        ret = (abs(x["deviation_value"])*100) / abs(x["lower_tol"])
    elif x["deviation_value"]<0 and x["lower_tol"]==0:
        ret = np.nan
    return ret


# Create your models here.
class MongoConnection(object):
    def __init__(self):
        client = MongoClient('localhost', 27017)
        bd = client['DB']
        self.collection = bd["Pieces"]

class Pieces(MongoConnection):
    def __init__(self):
        super(Pieces, self).__init__()
        self.pieces = None
        self.pieces2 = None
        self.pieces3 = None
        self.getDataAsDf()
        #self.pieces.to_csv("database.csv")  ## to generate .csv of query.
        #self.pieces2.to_csv("database2.csv")

    def getDataAsDf(self):
        """
        makes query in mongodb server to get and compose a dataframe to be procesed
        """
        # Query pieces perspective

        pipeline = [
            {
                "$project":
                    {
                        "_id": 1,
                        "prj_name": 1,
                        "piece_id": 1,
                        "piece_name": 1,
                        "CreationDate": 1,
                        "Status": 1,
                        "Operator": 1,
                        "controlNb": 1,
                        "measuredNb": 1,
                        "failedNb": 1,
                        "event": "$custom_properties.Event",
                        "line": "$custom_properties.Line",
                        "model": "$custom_properties.Model",
                        "NbMQSFailed": "$custom_properties.Nb MQS Failed",
                        "NbMQSPass": "$custom_properties.Nb MQS Pass",
                        "Supplier": "$custom_properties.Supplier",
                        "Unit": "$custom_properties.Unit",
                        "cp": "$spc.tolerances.cp",
                        "cpk": "$spc.tolerances.cpk",
                        "pp": "$spc.tolerances.pp",
                        "ppk": "$spc.tolerances.ppk",
                        "cg": "$spc.tolerances.cg",
                        "cgk": "$cgk.tolerances.cgk",
                        "measurementObjects": {"$size": "$measurement_objects"}
                    }
            }
        ]
        # Query controls perspective
        pipeline2 = [
            {"$unwind": "$measurement_objects"},
            {"$unwind": "$measurement_objects.measured_dimensions"},
            {
                "$project":
                    {
                        "id": 1,
                        "prj_name": 1,
                        "piece_id": 1,
                        "PieceName": 1,
                        "CreationDate": 1,
                        "Status": 1,
                        "Operator": 1,
                        "controlNb": 1,
                        "measuredNb": 1,
                        "failedNb": 1,

                        "object_id": "$measurement_objects.object_id",
                        "mo_name": "$measurement_objects.mo_name",
                        "mo_type": "$measurement_objects.mo_type",
                        "mo_measstatus": "$measurement_objects.mo_measstatus",

                        "dim_name": "$measurement_objects.measured_dimensions.dim_name",
                        "object_name": "$measurement_objects.measured_dimensions.object_name",
                        "nominal_value": "$measurement_objects.measured_dimensions.nominal_value",
                        "measured_value": "$measurement_objects.measured_dimensions.measured_value",
                        "deviation_value": "$measurement_objects.measured_dimensions.deviation_value",
                        "status": "$measurement_objects.measured_dimensions.status",
                        "control_id": "$measurement_objects.measured_dimensions.control_id",
                        "lower_tol": "$measurement_objects.measured_dimensions.lower_tol",
                        "lower_warn": "$measurement_objects.measured_dimensions.lower_warn",
                        "upper_tol": "$measurement_objects.measured_dimensions.upper_tol",
                        "upper_warn": "$measurement_objects.measured_dimensions.upper_warn",
                        "control_type": "$measurement_objects.measured_dimensions.control_type",
                        "dimension_type": "$measurement_objects.measured_dimensions.dimension_type"
                    }
            }
        ]
        pieces = self.collection.aggregate(pipeline2)
        pieces_list = Util.getCursorAsList(pieces)
        pieces = pd.DataFrame(pieces_list)
        pieces = pieces[pieces["Status"]!='Undefined']
        self.pieces = pieces

        pieces2 = self.collection.aggregate(pipeline)
        pieces2_list = Util.getCursorAsList(pieces2)
        pieces2 = pd.DataFrame(pieces2_list)
        self.pieces2 = pieces2

        # generate the final dataframe for data overview
        self.pieces3 = self.generatePiecesDf()

        return pieces, pieces2

    def generatePiecesDf(self):
        # quit undefined pieces
        df = self.pieces[self.pieces["Status"]!='Undefined']
        # cast to correct type of data
        df["deviation_value"] = df["deviation_value"].astype("float")
        # new columns for dataset
        df_grouped = (df.assign(**
        {    
            'passNb': df['controlNb']-df['failedNb'],
            # Controles pasados en general
            'posPastCtrls': df.apply(lambda x: True if (x["deviation_value"] > 0) & (x["deviation_value"] <= x["upper_tol"]) else False, axis = 1),
            # controles pasados positivos
            'posFailCtrls': df.apply(lambda x: True if (x["deviation_value"] > 0) & (x["deviation_value"] > x["upper_tol"]) else False, axis = 1),
            # controles fallados positivos
            'negPasslCtrls': df.apply(lambda x: True if (x["deviation_value"] < 0) & (x["deviation_value"] >= x["lower_tol"]) else False, axis = 1),
            # controles pasados negativos
            'negFaillCtrls': df.apply(lambda x: True if (x["deviation_value"] < 0) & (x["deviation_value"] < x["lower_tol"]) else False, axis = 1),
            #controles fallados negativos
            'absDeviation': df.apply(percent_deviation_control, axis = 1)
            #Desviación relativa
            
        }
        )
        .groupby('piece_id', as_index=False)
            .agg(
                prj_name = ('prj_name', 'first'), 
                Operator = ('Operator', 'first'), 
                Status = ('Status', 'first'), 
                CreationDate = ('CreationDate', 'first'), 
                perfectControls = ('deviation_value',  lambda x: (x == 0.0).sum()),
                posPassCtrls = ('posPastCtrls', 'sum'),
                negPassCtrls = ('negPasslCtrls', 'sum'),
                posFailCtrls = ('posFailCtrls', 'sum'),
                negFailCtrls = ('negFaillCtrls', 'sum'),
                absDeviation = ('absDeviation', 'mean'),
                ## absDeviation = ('absDeviation', 'mean'),
                maxDevPos = ('deviation_value', 'max'),
                maxDevNeg = ('deviation_value', 'min'),
                passNb =('passNb', 'first'),
                failedNb =('failedNb', 'first'),
                controlNb =('controlNb', 'first')
            )
        )
        # convert to percents
        df_grouped["perfectControls"] = (df_grouped["perfectControls"]*100)/df_grouped["controlNb"]
        df_grouped["posPassCtrls"] = (df_grouped["posPassCtrls"]*100)/df_grouped["controlNb"]
        df_grouped["negPassCtrls"] = (df_grouped["negPassCtrls"]*100)/df_grouped["controlNb"]
        df_grouped["posFailCtrls"] = (df_grouped["posFailCtrls"]*100)/df_grouped["controlNb"]
        df_grouped["negFailCtrls"] = (df_grouped["negFailCtrls"]*100)/df_grouped["controlNb"]
        # cast to correct dtypes
        df_grouped["CreationDate"] = pd.to_datetime(df_grouped["CreationDate"])
        df_grouped["day"] = df_grouped["CreationDate"].dt.weekday
        df_grouped["hour"] = df_grouped["CreationDate"].dt.hour
        df_grouped[["prj_name", "Operator", "Status"]] = df_grouped[["prj_name", "Operator", "Status"]].astype('category')

        return df_grouped


    def getAllPieces(self):
        """
        makes query mongodb to get all pieces
        :return: list of dictionaries with list of pieces.
        """

    def getInfoDataset(self, variable, valor, f_inicial, f_final): ## actualizado a pieces3
        """
        makes a pandas query to get general info about project in specific date range
        :param project: Name of project
        :param f_inicial: Initial date
        :param f_final: Final date
        :return: list of dictionaries with features of project dataset.
        """
        # copy to preserve original data structure
        df_local = self.pieces3.copy()

        # Preparing dates strings to filter
        try:
            # try to convert datetime range.
            start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
            # filtering by date range
            df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
        except:
            print("consulta sin rango de fecha")

        # filtering by project name
        df_local = df_local[df_local[variable] == valor]

        # validating dataset is not empty
        if len(df_local == 0):
            # generating variable summary
            result = Dm(df_local).datasetStatistics()
        else:
            result = {"status": 'FAIL', 'message': 'las variables de filtrado no proporcionan ninguna coincidencia'}

        return result  # lista_piezas.to_json(orient='records', default_handler=str)

    def getVariableDescr(self, variable, value, f_inicial, f_final, column):
        """
        makes query mongodb to get general info about project in specific date range
        :param project: Name of project
        :param f_inicial: Initial date
        :param f_final: Final date
        :param variable: variable in the database to describe
        :return: list of dictionaries with features of project dataset.
        """
        df_local = self.pieces3.copy()
        # Preparing dates strings to filter
        try:
            # try to convert datetime range.
            start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
            # filtering by date range
            df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
        except:
            print("consulta sin rengo de fecha")

        # filtering by condition
        df_local = df_local[df_local[variable] == value]

        # generating variable summary
        result = Dm(df_local).describeVar(column)
        return result  # lista_piezas.to_json(orient='records', default_handler=str)

    def getProjects(self, filter):
        """
        Makes query to get a list of total projects in database
        :return: list of projct
        """
        df_local = self.pieces.copy()
        agrupado = df_local.groupby(filter)[["CreationDate"]].agg(
            b_min=pd.NamedAgg(column="CreationDate", aggfunc="min"),
            b_max=pd.NamedAgg(column="CreationDate", aggfunc="max")
        ).reset_index()
        agrupado["b_min"] = agrupado["b_min"].dt.strftime('%Y-%m-%d')
        agrupado["b_max"] = agrupado["b_max"].dt.strftime('%Y-%m-%d')
        return agrupado.astype('object').to_dict(orient='records')

    def compareQC(self, project, f_inicial, f_final, variables):
        # clave_primaria, valor
        df_local = self.pieces

        # revisión de las variables a extraer
        valid_variables = False
        columns = [None, None]
        try:
            columns = variables.split(',')
            if columns[0].strip() in df_local.columns and columns[1].strip() in df_local.columns:
                valid_variables = ~valid_variables
        except BaseException as error:
            print("Indicaron al menos una variable que no existe en el dataframe")
            print(error)

        if valid_variables:
            # filtering by date
            try:
                # try to convert datetime range.
                start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
                # filtering by date range
                df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
            except:
                print("consulta sin rango de fecha")
            # filtering by project name
            df_local = df_local[df_local["prj_name"] == project]
            # Getting only variables selected
            df_local = df_local[[columns[0], columns[1]]]
            result = df_local.astype('object').to_dict(orient='list')
        else:
            result = {"status": 'FAIL',
                      'message': 'Se solicito al menos una variable que no existe',
                      "detail": variables}
        return result

    def DispVar(self, project, f_inicial, f_final, variables):
        df_local = self.pieces

        # revisión de las variables a extraer
        valid_variables = False
        columns = [None, None]
        try:
            columns = variables.split(',')
            if columns[0].strip() in df_local.columns and columns[1].strip() in df_local.columns:
                valid_variables = ~valid_variables
        except BaseException as error:
            print("Indicaron al menos una variable que no existe en el dataframe")
            print(error)

        if valid_variables:
            # filtering by date
            try:
                # try to convert datetime range.
                start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
                # filtering by date range
                df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]

            except:
                print("consulta sin rango de fecha")
            # filtering by project name
            df_local = df_local[df_local["prj_name"] == project]
            DateRange = df_local["CreationDate"]


            # Getting only variables selected
            df_dispvar = df_local[[columns[0], columns[1]]]
            df_dispvar = df_dispvar.join(DateRange)

            result = df_dispvar.astype('object').to_dict(orient='list')
        else:
            result = {"status": 'FAIL',
                      'message': 'Se solicito al menos una variable que no existe',
                      "detail": variables}
        return result

    def getCountsByOperator(self, project, f_inicial, f_final):
        """
        makes a pandas query to get the counts of approved and rejected in specific date range
        :param project: Name of project
        :param f_inicial: Initial date
        :param f_final: Final date
        :return: list of dictionaries with counts of approved and rejected by operator.
        """
        # copy to preserve original data structure
        df_local = self.pieces3.copy()

        # Preparing dates strings to filter
        try:
            # try to convert datetime range.
            start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
            # filtering by date range
            df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
        except:
            print("consulta sin rango de fecha")

        # filtering by project name
        df_local = df_local[df_local["prj_name"] == project]

        # generating required arrays
        aux = df_local.groupby("Operator")[["Status"]].agg(
            {'Status': [("Approved", lambda x: (x=='Approved').sum()),("Rejected", lambda x: (x=='Rejected').sum())]}
        )

        aux.columns = aux.columns.droplevel()

        aux['Operator'] = aux.index.values

        result = aux.astype('object').to_dict(orient='list')

        #result = {"Opetators": aux.index.values,"Approved": aux.Approved.values, "Rejected": aux.Rejected.values}

        print(result)

        return result

    def getCountsByProject(self, operator, f_inicial, f_final):
        # copy to preserve original data structure
        df_local = self.pieces3.copy()

        # Preparing dates strings to filter
        try:
            # try to convert datetime range.
            start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
            # filtering by date range
            df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
        except:
            print("consulta sin rango de fecha")

        # filtering by project name
        df_local = df_local[df_local["Operator"] == operator]

        # generating required arrays
        aux = df_local.groupby("prj_name")[["Status"]].agg(
            {'Status': [("Approved", lambda x: (x=='Approved').sum()),("Rejected", lambda x: (x=='Rejected').sum())]}
        )

        aux.columns = aux.columns.droplevel()

        aux['prj_name'] = aux.index.values

        result = aux.astype('object').to_dict(orient='list')

        #result = {"Opetators": aux.index.values,"Approved": aux.Approved.values, "Rejected": aux.Rejected.values}

        print(result)

        return result

    def getCountsByStatus(self, Status, f_inicial, f_final):
        # copy to preserve original data structure
        df_local = self.pieces3.copy()

        # Preparing dates strings to filter
        try:
            # try to convert datetime range.
            start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
            # filtering by date range
            df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
        except:
            print("consulta sin rango de fecha")

        # filtering by project name
        df_local = df_local[df_local["Status"] == Status]

        # generating required arrays
        """aux = df_local.groupby("prj_name")[["Operator"]].agg({
        'Operator': [("TRICIA", lambda x: (x=='TRICIA').sum()),
                    ("KRAIG", lambda x: (x=='KRAIG').sum()),
                    ("DAN", lambda x: (x=='DAN').sum()),
                    ("FRED", lambda x: (x=='FRED').sum()),
                    ("SHAWN", lambda x: (x=='SHAWN').sum()),
                    ("JILL", lambda x: (x=='JILL').sum()),
                    ("SCOTT", lambda x: (x=='SCOTT').sum()),
                    ("JESSE", lambda x: (x=='JESSE').sum()),
                    ("CODY", lambda x: (x=='CODY').sum()),
                    ("ZACHARY", lambda x: (x=='ZACHARY').sum()),
                    ("BOB", lambda x: (x=='BOB').sum()),
                    ("GARY", lambda x: (x=='GARY').sum())
            ]
        })"""

        
        aux = df_local.groupby("Operator")[["prj_name"]].agg({
        'prj_name': [("CONFIDENTIAL 65720 TVA KTH", lambda x: (x=='CONFIDENTIAL 65720 TVA KTH').sum()),
                    ("CONFIDENTIAL R60810 TVA_TWA Frt Side Frame", lambda x: (x=='CONFIDENTIAL R60810 TVA_TWA Frt Side Frame').sum())
        ]
        })

        aux.columns = aux.columns.droplevel()

        aux['Operator'] = aux.index.values

        result = aux.astype('object').to_dict(orient='list')

        #result = {"Opetators": aux.index.values,"Approved": aux.Approved.values, "Rejected": aux.Rejected.values}

        print(result)

        return result

    """
    PATTERN DISCOVERY METHODS
    """

    def timeSerieAnalysis(self, project):
        df_local = self.pieces
        # filtering by project name
        df_local = df_local[df_local["prj_name"] == project]
        df_analysis = Dm(df_local)
        result = df_analysis.timeSeriesAnalysis(project)
        return result

    def kPrototypeClustering(self, project):
        df_local = self.pieces2
        df_local = df_local[df_local["prj_name"] == project]
        df_analysis = Dm(df_local)
        result = df_analysis.kprototypesclustering(project)
        return result

    def aprioriAsociationRules(self, project):
        df_local = self.pieces
        df_analysis = Dm(df_local)
        result = df_analysis.getAssociationRules(project)
        return result
    
    def decisionTree(self):
        df_local = self.pieces.copy()
        df_analysis = Dm(df_local)
        result = df_analysis.getDecisionRules()
        return result

    def getTimeSeries(self, filter, value, f_inicial, f_final):
        ## agrupar el df por fecha y contar los controles fallidos/aprobados por día.
        ts = self.pieces3.copy()
        # filter variable
        ts = ts[ts[filter] == value]
        print(ts.info())
        # filter date range
        start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
        ts = ts[(ts["CreationDate"] >= start) & (ts["CreationDate"] <= end)]
        # create and resample dataframe to timeserie
        ts.set_index('CreationDate', inplace=True)
        ts_pass = ts["Status"]
        ts_rs_pass = ts_pass.resample('D').agg(lambda x: (x=='Approved').sum())
        ts_rs_fail = ts_pass.resample('D').agg(lambda x: (x=='Rejected').sum())

        return {
            "index": ts_rs_pass.dropna().index.strftime("%d-%m-%Y").tolist(),
            "serie": ts_rs_pass.dropna().astype('object').tolist(),
            "serie_f": ts_rs_fail.dropna().astype('object').tolist(),
        }

    def getHeatmapD(self, project):
        df_local = self.pieces
        # filtering by project name
        df_local = df_local[df_local["prj_name"] == project]
        # pivot dataframe
        col_names = ["Control "+str(name) for name in range(1, df_local["control_id"].nunique()+1)]
        df_local = df_local.pivot(index='piece_id', columns='control_id', values='deviation_value').reset_index()
        df_local.set_index("piece_id", inplace=True)
        # Se renombran las columnas
        df_local.columns = col_names
        mat_corr = df_local.astype('float').corr()
        mat_corr.fillna(0, inplace=True)
        result = mat_corr.to_dict(orient='split')
        return result
