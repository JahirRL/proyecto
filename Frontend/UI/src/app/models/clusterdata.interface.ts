export interface ClusterData {
    x: Array<string>,
    y: Array<string>,
    z: Array<number>,
    clusters: Array<number>,
    centroid1: Array<string>,
    centroid2: Array<string>,
    centroid3: Array<string>,
    centroid4: Array<string>,
}
