# Pyton data analysis library
import pandas as pd


from pandas.api.types import is_numeric_dtype
from pandas.api.types import is_categorical_dtype
from pandas.api.types import is_object_dtype
from pandas.api.types import is_datetime64_dtype
from pandas.api.types import is_datetime64_ns_dtype

from mlxtend.frequent_patterns import association_rules, apriori
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

from sklearn.tree import DecisionTreeClassifier

import statsmodels.api as sm
from statsmodels.tsa.stattools import adfuller

from .tvakth import *
from .frtside import *

# K-Prototypes
import numpy as np
from kmodes.kprototypes import KPrototypes
from sklearn.neighbors import LocalOutlierFactor
from sklearn.model_selection import train_test_split
pd.set_option('display.float_format', lambda x: '%.3f' % x)



def describeNumerical(s):
    """
    describes a categorical variable in the form of series
    :param s: Series of variable
    :return: dictionary with descriptive elements of the variable
    """
    disctint = int(s.nunique())
    percent_unique = float((disctint * 100) / s.size) if s.size > 0 else 0
    missing = int(s.isnull().sum())
    percent_missing = float((missing * 100) / s.size) if s.size > 0 else 0
    statistics = s.describe().to_dict()
    statistics["uniques"] = disctint
    statistics["% uniques"] = percent_unique
    statistics["missing"] = missing
    statistics["% missing"] = percent_missing
    statistics["values"] = s.to_list()
    return statistics


def describeCategorical(s):
    """
    describes a categorical variable in the form of series
    :param s: Series of variable
    :return: dictionary with descriptive elements of the variable
    """
    disctint = int(s.nunique())
    percent_unique = float((disctint * 100) / s.size) if s.size > 0 else 0
    missing = int(s.isnull().sum())
    percent_missing = float((missing * 100) / s.size) if s.size > 0 else 0
    values = s.value_counts().reset_index().rename(columns={'index': s.name, s.name: 'counts'}).to_dict(orient='list')
    return {
        "uniques": disctint,
        "% uniques": percent_unique,
        "missing": missing,
        "% missing": percent_missing,
        "categories": values
    }

def describeDatetime(s):
    """
    describes a datetime64[ns] variable in the form of series
    :param s: Series of variable
    :return: dictionary with descriptive elements of the variable
    """
    # first we need to get mont and year and day
    dates = s.dt.date
    # after we need to get the min date and max date
    min_date = dates.min()
    max_date = dates.max()
    num_days = (max_date - min_date).days
    values = dates.value_counts().reset_index().rename(columns={'index': s.name, s.name: 'counts'}).\
        sort_values(by=s.name).to_dict(orient='list')
    return {
        "date start": min_date,
        "date end": max_date,
        "num days": num_days,
        "categories": values
    }


class Stationarity:
    def __init__(self,  significance=0.05):  ## la hipotesis nula es que no son estacionarios, la hipótesis alternativa es que si es estacionaria
        self.significance_level = significance
        self.pvalue = None
        self.is_stationarity = None

    def ADF(self, values):  # augmented dicky fuller
        result = adfuller(values, autolag='AIC')  #
        format_results = pd.Series(result[:4], index=['Statistic', 'P-value', 'Num of Lags', 'Observations'])
        for k, v in result[4].items():
            format_results[f'Crical Value {k}'] = v
        return format_results

class Dm:
    def __init__(self, my_df):
        self.df = my_df

    def datasetStatistics(self):
        """
         calculates and returns the statistics of a dataframe in a dictionary
        """
        number_var = len(self.df.columns)
        b_min = self.df.CreationDate.min().strftime('%Y-%m-%d')
        b_max = self.df.CreationDate.max().strftime('%Y-%m-%d')
        number_obs = len(self.df)
        number_cells = self.df.size
        atributes_missing = self.df.isnull().sum()
        number_missing = atributes_missing.sum()
        percent_missing = (number_missing * 100) / number_cells
        sample = self.df.sample(n=6, random_state=16).astype('object').iloc[:, 1:].to_dict(orient='records')

        return {'variables': int(number_var),
                'observations': int(number_obs),
                'd_min': b_min,
                'd_max': b_max,
                'cells': int(number_cells),
                'missing': int(number_missing),
                'missingXatribute': atributes_missing.to_dict(),
                'percntMissing': float(percent_missing),
                'sampleData': sample}

    def datasetVariables(self, parse_objects=True):
        """
        calculates descriptions of all variables in the data set and returns a dictionary.
        input parameters:
            parse_objects - indicates whether object-type attributes will be converted to categories.
        """
        if parse_objects:
            columns_obj = list(self.df.select_dtypes(['object']).columns)  # get columns object-type
            for column in columns_obj:
                self.df[column] = self.df[column].astype('category')  # convert object to category
        # start atribute analysis
        d = {}
        for column in self.df.columns:
            if is_categorical_dtype(self.df[column]):
                d[column] = describeCategorical(self.df[column])
                # WARNING THIS FAILS IF ARE TWO OR MORE COLUMNS WITH THE SAME NAME
            elif is_numeric_dtype(self.df[column]):
                d[column] = describeNumerical(self.df[column])
                # WARNING THIS FAILS IF ARE TWO OR MORE COLUMNS WITH THE SAME NAME
        """
        Warning: the use of dictionaries can be costly in memory, this can be improved by using NumPy compound arrays
        and directly choosing the length of the data types, in this way if you want to store an array of integers with 
        4 digits you would only use an int8 and not an int64 (by default).
        """
        return d

    def describeVar(self, var_name):
        result = {"status": "FAIL", "message": "Se intentó procesar un tipo de dato no reconocido"}
        if var_name in self.df.columns:
            var_type = self.df.dtypes[var_name]
            print(var_type)
            if is_categorical_dtype(var_type):
                result = describeCategorical(self.df[var_name])
            elif is_object_dtype(var_type):
                try:
                    self.df[var_name] = self.df[var_name].astype('category')  # convert object to category
                    result = describeCategorical(self.df[var_name])
                except BaseException:
                    result["message"] = "Se intentó procesar una variable no categórica com categórica"
            elif is_numeric_dtype(var_type):
                result = describeNumerical(self.df[var_name])
            elif is_datetime64_ns_dtype(var_type) or is_datetime64_dtype(var_type):
                result = describeDatetime(pd.to_datetime(self.df[var_name], "%Y-%m-%dT%H:%M:%S"))
        else:
            result = {"status": "FAIL", "message": "La variable indicada no existe en la base de datos"}
        return result

    def getAssociationRules(self, project):
        """
        Asociation rules of project
        generate list of asociation rules in the dataset
        param min_support: f/#transacctions
        param variables:  list of variables to get asociation rules
        param number_rules: number of rules to return
        :return: dictionary with asociation rules
        """

        df = self.df.copy()
        b_min = df.CreationDate.min().strftime('%Y-%m-%d')
        b_max = df.CreationDate.max().strftime('%Y-%m-%d')
        if project == 'CONFIDENTIAL R60810 TVA_TWA Frt Side Frame':
            df = df[df["prj_name"]=="CONFIDENTIAL R60810 TVA_TWA Frt Side Frame"]
            dict = {'Pass': False, 'Fail': True}
            sample_size = 1000
            indices = [0, 3, 6, 9, 10]
            str = "In %0.2f %% of the measured pieces the controls %s failed the tolerance and in %0.2f %% of this sample when the control(s) %s failed the control(s) %s they also failed."
            

        elif project == 'CONFIDENTIAL 65720 TVA KTH':
            df = df[df["prj_name"]=="CONFIDENTIAL 65720 TVA KTH"]
            dict = {'Pass': True, 'Fail': False}
            sample_size = 250
            indices = [147, 100, 342, 237, 385]
            str = "In %0.2f %% of the measured pieces the controls %s passed the tolerance and in %0.2f %% of this sample when the control(s) %s passed the control(s) %s they also passed."
        
        df_pieces = df.groupby("piece_id")[["Status", "failedNb"]].first()
        df_balanced = df[df["piece_id"].isin(df_pieces.nlargest(sample_size, "failedNb").index.to_list())]
        df_controls = df_balanced[["piece_id", "control_id", "status"]]
        df_controls = df_controls.astype('category')
        df_controls["control"] = df_controls["control_id"].cat.codes
        df_controls.drop(["control_id"], inplace=True, axis=1)
        df_controls["status"].replace(dict, inplace=True)
        df_pivot = df_controls.pivot(index='piece_id', columns='control', values='status').reset_index()
        df_pivot.drop(["piece_id"], axis=1,inplace=True)

        frequent_patterns = apriori(df_pivot, min_support=0.7, use_colnames=True)
        rules = association_rules(frequent_patterns, metric="confidence", min_threshold=0.7)
        #rules = rules[rules.lift>1].head(9)[["antecedents", "consequents", "support", "confidence"]]
        rules = rules[rules.index.isin(indices)]
        rules = rules[["antecedents", "consequents", "confidence", "support"]]
        rules["antecedents"] = rules["antecedents"].apply(lambda x: list(x)).astype("unicode")
        rules["consequents"] = rules["consequents"].apply(lambda x: list(x)).astype("unicode")

        ## GENERATING Natural text

        nl_rules = []
        for i in indices:
            nl_rules.append(str%(rules.loc[i, "support"]*100, 
            rules.loc[i, "antecedents"]+rules.loc[i, "consequents"], 
            rules.loc[i, "confidence"]*100, 
            rules.loc[i, "antecedents"], 
            rules.loc[i, "consequents"]
            ))
        
        number_var = len(df_pivot.columns)
        number_obs = len(df_pivot)

        model_dict = rules.astype('object').to_dict(orient="records")
        stats_dict = {'variables': int(number_var),
                'observations': int(number_obs),
                'd_min': b_min,
                'd_max': b_max,
                'natural_rules': nl_rules,
                'model': model_dict}
        return stats_dict

    def getDecisionRules(self):
        df = self.df
        b_min = df.CreationDate.min().strftime('%Y-%m-%d')
        b_max = df.CreationDate.max().strftime('%Y-%m-%d')
        # Project selection
        df = df[df["prj_name"]=='CONFIDENTIAL R60810 TVA_TWA Frt Side Frame']
        # dataframe grouped by pieces
        df_pieces = df.groupby('piece_id')[["CreationDate", "Operator", "Status", "failedNb"]].first()
        # delete undefined pieces Status
        df_pieces = df_pieces[df_pieces["Status"]!="Undefined"]
        # get sample of majority class
        df_pieces_approved = df_pieces[df_pieces["Status"] == "Approved"]
        # get index of subsample approved pieces
        approved_index = df_pieces_approved.sample(400, random_state=16).index
        # get index of subsample rejected pieces
        rejected_index = df_pieces[df_pieces["Status"] == "Rejected"].index
        # merge the index
        final_index = approved_index.append(rejected_index)
        # select the rows of genral dataframe by index
        df_balanced = df[df["piece_id"].isin(final_index.to_list())]
        # before pivot, rename columns of the sample
        col_names = ["Control "+str(name) for name in range(1, df_balanced["control_id"].nunique()+1)]
        ## change to numeric the column status
        df_balanced["status"].replace({'Pass': 1, 'Fail': 0}, inplace=True)
        # pivot dataframe
        df_controls = df_balanced.pivot(index='piece_id', columns='control_id', values='deviation_value')
        # renaming columns
        df_controls.columns = col_names
        # expanding dataframe to pieces atribute perspective
        df_pieces = df_balanced.groupby("piece_id")[["CreationDate", "Status", "Operator"]].first()
        # merging dataframes
        df_final = df_pieces.join(df_controls, how="inner", on="piece_id")
        # cast to correct dtypes.
        df_final["CreationDate"] = pd.to_datetime(df_final["CreationDate"])
        df_final["Status"] = df_final["Status"].astype("category") 
        # dummify dataframe
        category_part = df_final[["CreationDate", "Operator"]]
        # decompose CreationDate attribute
        category_part = category_part.assign(
            day = category_part["CreationDate"].dt.weekday,
        # hour = category_part["CreationDate"].dt.hour
        )
        category_part.drop(["CreationDate"], inplace=True, axis=1)
        dict = {0: "Monday", 1: "Tuesday", 2: "Wednesday", 3: "Thursday", 4: "Friday", 5: "Saturday", 6:"Sunday" }
        category_part.replace({"day": dict}, inplace = True)
        cat_part_dumm = pd.get_dummies(category_part)
        final_df = pd.concat([cat_part_dumm, df_final.iloc[:, 3:], df_final[["Status"]]], axis=1)
        labelencoder = LabelEncoder()
        final_df['Status'] = labelencoder.fit_transform(final_df['Status'])

        ### MODEL TRAIN

        y = final_df['Status']
        X = final_df.drop('Status', axis=1)
        X_train, X_test, y_train, y_test = train_test_split(X, y,
            test_size=0.2, 
            random_state=16)
        
        profundidad = 5
        #Entrenamiento del modelo
        clasificador = DecisionTreeClassifier( max_depth=profundidad,
                                            criterion="entropy", 
                                            random_state=0)
        clasificador.fit(X_train, y_train)

        weights = pd.Series(clasificador.feature_importances_,
                    index=X.columns.values)

        number_var = len(final_df.columns)
        number_obs = len(final_df)

        feature_importance = weights.sort_values(ascending=False).head(10).index.to_list()
        stats_dict = {'variables': int(number_var),
                'observations': int(number_obs),
                'd_min': b_min,
                'd_max': b_max,
                'feature_importance': feature_importance}
        return stats_dict

    def timeSeriesAnalysis(self, prj_name):
        ts = self.df.copy()

        b_min = ts.CreationDate.min().strftime('%Y-%m-%d')
        b_max = ts.CreationDate.max().strftime('%Y-%m-%d')

        # filter project
        ts = ts[ts["prj_name"] == prj_name]
        # cast de string a datetime64[ns]
        ts.CreationDate = pd.to_datetime(ts.CreationDate)
        # Creación de la serie de tiempo
        ts.set_index('CreationDate', inplace=True)
        # eliminación de todas las columnas menos status
        ts = ts["status"]
        # Resampleo de la serie de tiempo por día con una agregación de porcentaje en status
        ts_rs = ts.resample('D').agg(lambda x: (x == 'Fail').mean())
        # Test de dickey fuller para comprobar la estacionaridad
        adf_test = Stationarity()
        result = adf_test.ADF(ts_rs.fillna(ts_rs.mean()).values).round(6)
        estacionaria = True if result["Statistic"] < result["Crical Value 1%"] else False
        # se realiza la descomposición y se obtienen las componentes de la serie.
        decomposition = sm.tsa.seasonal_decompose(ts_rs.dropna(), model='additive', period=50)

        number_obs = len(ts_rs)
        return {
            'observations': int(number_obs),
            'd_min': b_min,
            'd_max': b_max,
            "index": ts_rs.dropna().index.strftime("%d-%m-%Y").tolist(),
            "serie": ts_rs.dropna().astype('object').tolist(),
            "trend": decomposition.trend.fillna(0).astype('object').tolist(),
            "seasonality": decomposition.seasonal.astype('object').tolist(),
            "stationarity": estacionaria,
            "nl_conclusion": "The percentages of failed controls per day suggest the behavior of a stationary phenomenon with 90% confidence. There is no clear reason for increase or decrease, the seasonality of the observations does not have a sequence, this means that, in general, there is no pattern of behavior in the percentages of failed controls over time with a daily frequency."
        }

    def kprototypesclustering(self, prj_name):
        dt = self.df.copy()

        # Select relevant columns
        cols = ['prj_name', 'CreationDate', 'Status', 'Operator', 'measuredNb', 'failedNb',
                'NbMQSFailed', 'NbMQSPass', 'Unit']
        dt2 = dt[cols].copy()

        # Select date range
        initial_date = dt.CreationDate.min().strftime('%Y-%m-%d')
        final_date = dt.CreationDate.max().strftime('%Y-%m-%d')

        # Convert cols numericas a float (KPrototypes requirement)
        cols_num = dt2._get_numeric_data().columns.tolist()
        dt2[cols_num] = dt2[cols_num].astype(float)

        # Convert dataframe to numpy array (KPrototypes requirement)
        dt2_array = dt2.values

        if prj_name == 'CONFIDENTIAL 65720 TVA KTH':

            # Se definió que 4 clusters es adecuado para clusterizar
            kproto = KPrototypes(n_clusters=4, init = 'Huang',  random_state=0)

            # Especificar índices de variables categóricas
            clusters = kproto.fit_predict(dt2_array, categorical=[0, 1, 2, 3, 8])

            # Obtener los centroides
            centroides = kproto.cluster_centroids_

            # Crear columna con clusters
            cluster_dict = []
            for c in clusters:
                cluster_dict.append(c)

            # Añadir columna de clusters a dataframe
            dt2['Cluster'] = cluster_dict

            return {"Initial date": initial_date,
                    "Final date": final_date,
                    "Variables": len(cols),
                    "Observations": dt2.shape[0],
                    "x": dt2['CreationDate'].tolist(),
                    "y": dt2['Operator'].tolist(),
                    "z": dt2['failedNb'].tolist(),
                    "clusters": clusters.tolist(),
                    "centroids": centroides.tolist(),
                    "Outliers": "None",
                    "Conclusion": "This project only contains pieces approved. No outliers were detected."}

        elif prj_name == 'CONFIDENTIAL R60810 TVA_TWA Frt Side Frame':
            # Replace null values by 'missing' or 0
            dt2['Operator'].fillna('missing', inplace=True)
            dt2['Unit'].fillna('missing', inplace=True)

            # Se definió que 3 clusters es adecuado para clusterizar
            kproto = KPrototypes(n_clusters=3, init = 'Huang', random_state=0)

            # Especificar índices de variables categóricas
            clusters = kproto.fit_predict(dt2_array, categorical=[0, 1, 2, 3, 8])

            # Obtener los centroides
            centroides = kproto.cluster_centroids_

            # Crear columna con clusters
            cluster_dict = []
            for c in clusters:
                cluster_dict.append(c)

            # Añadir columna de clusters a dataframe
            dt2['Cluster'] = cluster_dict

            # Only one outlier was found, at cluster 0
            col_outlier = dt2.loc[dt2['Status'] == 'Undefined'].columns.tolist()
            val_outlier = dt2.loc[dt2['Status'] == 'Undefined'].values.tolist()


            return {"Initial date": initial_date,
                    "Final date": final_date,
                    "Variables": len(cols),
                    "Observations": dt2.shape[0],
                    "x": dt2['CreationDate'].tolist(),
                    "y": dt2['Operator'].tolist(),
                    "z": dt2['failedNb'].tolist(),
                    "clusters": clusters.tolist(),
                    "centroids": centroides.tolist(),
                    "Column outlier": col_outlier,
                    "Value outlier": val_outlier,
                    "Conclusion": "The outlier detected corresponds to the only piece with 'Undefined' status"}




