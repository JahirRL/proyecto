# Pyton data analysis library
from pandas.api.types import is_numeric_dtype
from pandas.api.types import is_categorical_dtype
from mlxtend.frequent_patterns import association_rules, apriori
import pandas as pd
import json


def describe_numerical(s):
    """
    describes a categorical variable in the form of series
    :param s: Series of variable
    :return: dictionary with descriptive elements of the variable
    """
    disctint = s.nunique()
    percent_unique = (disctint * 100) / s.size
    missing = s.isnull().sum()
    percent_missing = (missing * 100) / s.size
    statistics = s.describe().to_dict()
    statistics["uniques"] = disctint
    statistics["% uniques"] = percent_unique
    statistics["missing"] = missing
    statistics["% missing"] = percent_missing
    statistics["values"] = s.to_list()
    return statistics


def describe_categorical(s):
    """
    describes a categorical variable in the form of series
    :param s: Series of variable
    :return: dictionary with descriptive elements of the variable
    """
    disctint = s.nunique()
    percent_unique = (disctint * 100) / s.size
    missing = s.isnull().sum()
    percent_missing = (missing * 100) / s.size

    return {
        "uniques": disctint,
        "% uniques": percent_unique,
        "missing": missing,
        "% missing": percent_missing,
        "categories": s.nunique()
    }


class Dm:
    def __init__(self, my_df):
        self.df = my_df

    def dataset_statistics(self):
        """
         calculates and returns the statistics of a dataframe in a dictionary
        """
        number_var = len(self.df.columns)
        number_obs = len(self.df)
        number_cells = self.df.size
        atributes_missing = self.df.isnull().sum()
        number_missing = atributes_missing.sum()
        percent_missing = (number_missing * 100) / number_cells

        return {'variables': number_var,
                'observations': number_obs,
                'cells': number_cells,
                'missing': number_missing,
                'missing per atribute': atributes_missing.to_dict(),
                '% missing': percent_missing}

    def dataset_variables(self, parse_objects=True):
        """
        calculates descriptions of all variables in the data set and returns a dictionary.
        input parameters:
            parse_objects - indicates whether object-type attributes will be converted to categories.
        """
        if parse_objects:
            columns_obj = list(self.df.select_dtypes(['object']).columns)  # get columns object-type
            for column in columns_obj:
                self.df[column] = self.df[column].astype('category')  # convert object to category
        # start atribute analysis
        d = {}
        for column in self.df.columns:
            if is_categorical_dtype(self.df[column]):
                d[column] = describe_categorical(self.df[column])
                # WARNING THIS FAILS IF ARE TWO OR MORE COLUMNS WITH THE SAME NAME
            elif is_numeric_dtype(self.df[column]):
                d[column] = describe_numerical(self.df[column])
                # WARNING THIS FAILS IF ARE TWO OR MORE COLUMNS WITH THE SAME NAME
        """
        Warning: the use of dictionaries can be costly in memory, this can be improved by using NumPy compound arrays
        and directly choosing the length of the data types, in this way if you want to store an array of integers with 
        4 digits you would only use an int8 and not an int64 (by default).
        """
        return d

    def getAsociationRules(self):
        """
        BETA VERSION static asociation rules
        generate list of asociation rules in the dataset
        :param min_support: f/#transacctions
        :param variables:  list of variables to get asociation rules
        :param number_rules: number of rules to return
        :return: dictionary with asociation rules
        """

        df = self.df[["Engine Cylinders", "Driven_Wheels", "Vehicle Size", "Number of Doors", "MSRP"]]
        df = df.dropna()
        df["Engine Cylinders"] = pd.cut(df["Engine Cylinders"], 3, labels=["Bajo", "Medio", "Alto"])
        df["Number of Doors"] = df["Number of Doors"].astype('category')
        df["Number of Doors"] = df["Number of Doors"].replace({2.0: "two", 3.0: "three", 4.0: "four"})
        df = df[df["MSRP"] < 100000]
        df["MSRP"] = pd.cut(df["MSRP"], 3, labels=["Bajo", "Medio", "Alto"])
        df = pd.get_dummies(df)
        ap = apriori(df, min_support=0.01, max_len=2, use_colnames=True)
        rules = association_rules(ap, min_threshold=0.01)
        final = rules.sort_values("lift", ascending=False).head(10).reset_index()
        final["antecedents"] = final["antecedents"].apply(lambda x: list(x)[0]).astype("unicode")
        final["consequents"] = final["consequents"].apply(lambda x: list(x)[0]).astype("unicode")
        return final.to_json()
