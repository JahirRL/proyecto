//----------------General---------------//
import { formatDate } from '@angular/common';
import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ElementRef, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
//--------------------------------------//

/*-----------Pattern discovery-----------*/
import { Project } from 'src/app/models/project.interface';
import { Infoproject } from 'src/app/models/infoproject.interface';
import { Timeseries } from 'src/app/models/timeseries.interface';
import { ClusterData } from 'src/app/models/clusterdata.interface';
import { MatTableDataSource } from '@angular/material/table';
import { AssociationRules } from 'src/app/models/associationrules.interface';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Clipboard } from '@angular/cdk/clipboard';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import dateFormat  from 'dateformat';
import { PlotlyService } from 'angular-plotly.js';
import { Platform } from '@angular/cdk/platform';
import { MAssociationRules } from 'src/app/models/modelassociationrules.interface';
import { from } from 'rxjs';
//--------------------------------------//

//(1) Local storage code
/*
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
*/

//-----------Pattern discovery-----------//
export interface DialogData {
  message: string;
}

export interface DialogMData {
  smodel: string;
}

export interface ProjectsOp {
  projects: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit, AfterViewInit {

  constructor(private api:ApiService,
    public modelD: MatDialog,
    public columnD: MatDialog,
    private location: Location,
    private clipboard: Clipboard,
    private activatedRoute: ActivatedRoute,
    private platform: Platform,
    public plotlyService: PlotlyService,
    public modelED: MatDialog){

      this.location = location;

    }

   //-------------Data overview-------------//
   @ViewChild('divRem') divRem!: ElementRef;
   @ViewChild(MatAccordion) accordion!: MatAccordion;
   @ViewChild('imprimeDO') imprimeDO!: ElementRef;
   @ViewChild('imprimirCOD') imprimeCOD!: ElementRef;
   @ViewChild('imprimirCOT') imprimeCOT!: ElementRef;
 
   displayedColumns: string[] = [];
   
   years = [
     {year: "2018", value: "2018"},
     {year: "2019", value: "2019"},
     {year: "2020", value: "2020"},
     //{year: "2021", value: "2021"},
   ];

   months = [
     {month: "January", value: new Date().getFullYear()+"-01-" },
     {month: "February", value: new Date().getFullYear()+"-02-" },
     {month: "March", value: new Date().getFullYear()+"-03-" },
     {month: "April", value: new Date().getFullYear()+"-04-" },
     {month: "May", value: new Date().getFullYear()+"-05-" },
     {month: "June", value: new Date().getFullYear()+"-06-" },
     {month: "July", value: new Date().getFullYear()+"-07-" },
     {month: "August", value: new Date().getFullYear()+"-08-" },
     {month: "September", value: new Date().getFullYear()+"-09-" },
     {month: "October", value: new Date().getFullYear()+"-10-" },
     {month: "November", value: new Date().getFullYear()+"-11-" },
     {month: "December", value: new Date().getFullYear()+"-12-" }
   ];
 
   columns = [
     {column: "% Perfect controls", value: "perfectControls"},
     {column: "% Passed controls +", value: "posPassCtrls"},
     {column: "% Passed controls -", value: "negPassCtrls"},
     {column: "% Failed controls +", value: "posFailCtrls"},
     {column: "% Failed controls -", value: "negFailCtrls"},
     {column: "Average deviation", value: "absDeviation"},
     {column: "Maximum deviation +", value: "maxDevPos"},
     {column: "Maximum deviation -", value: "maxDevNeg"},
     {column: "Passed controls", value: "passNb"},
     {column: "Failed controls", value: "failedNb"},
     {column: "Day", value: "day"},
     {column: "Hour", value: "hour"}
   ]
 
   public graph = {
     data: [
       { x: [''], y: [0], name: '', marker: { color: "", }, type: 'bar' },
       { x: [''], y: [0], name: '', marker: { color: "", }, type: 'bar' }
     ],
     layout: { title: ''},
     config: {responsive: false}
   };
 
   public graphHistogramG1 = {
     data: [
       { x: [0], marker: { color: "", }, type: 'bar' },
       { x: [''], y: [0], marker: { color: "", }, type: 'bar' }
     ],
     layout: { title: ''},
     config: {responsive: false}
   };
 
   public graphHistogramG2 = {
     data: [
       { x: [0], marker: { color: "", }, type: 'bar' },
       { x: [''], y: [0], marker: { color: "", }, type: 'bar' }
     ],
     layout: { title: ''},
     config: {responsive: false}
   };
   
   public graphPie1 = {
     data: [
       { values: 0, labels: [''], type: 'pie' },
     ],
     layout: {title: ''},
     config: {responsive: false}
   };
 
   public graphPie2 = {
     data: [
       { values: 0, labels: [''], type: 'pie' },
     ],
     layout: {title: ''},
     config: {responsive: false}
   };
 
   public graph2 = {
     data: [
       { values: 0, labels: [''], type: 'pie' },
     ],
     layout: {title: ''},
     config: {responsive: false}
   };
   
   public graph3 = {
     
     data: [
       {x: [''], y: [''], name: '', marker: { color: '', }, type: 'scatter'},
       {x: [''], y: [''], name: '', marker: { color: '', }, type: 'scatter'}
     ],
     layout: {
       title: '',
       yaxis: {
         title: "",
       }
     },
     config: {responsive: false}
   };
 
   public currentDate: String = dateFormat(new Date(), "yyyy-mm-dd");
   public startProjectDate!: String;
   public endProjectDate!: String;
   public projectNameDO!: String;
   public startDate!: String;
   public variable!: String;
   public endDate!: String;
   public column1!: String;
   public column2!: String;
   public value!: String;
   private verVari!: boolean;
   public verValues!: boolean;
   private verstDateDO!: boolean;
   private verenDateDO!: boolean;
   private stYear!: string;
   private enYear!: string;
   private datatemp: any;

   public staticName!: String;
   public staticImage!: String;

   public histogramName1!: string;
   public histogramName2!: string;

   public completed: boolean = false;
   public completed2: boolean = false;
   
   public isvisibleWelMCard: boolean = true;
   public isvisibleMCard: boolean = false;
   public isDisabled: boolean = true;
 
   public isVariableVisible: number = 1;
   public isPeriodVisible: number = 1;
   public j: number = 0;
   
   private infoProject!: any[];
   public operatorsCounts:any=[];
   public dataOverview: any= [];
   public pieChartData1:any=[];
   public pieChartData2:any=[];
   public variablesData:any=[];
   public tableData: any = [];
   public periodData:any=[];
   public valuesGH: any=[];
   public optionsP!: any[];
   public optionsO!: any[];
   public optionsS!: any[];
 
   getDataForm = new FormGroup({
     variable: new FormControl(),
     operator: new FormControl(),
     project: new FormControl(),
     d_start: new FormControl(),
     status: new FormControl(),
     d_end: new FormControl(),
     year: new FormControl()
   })
 
   getDataFormColumn1 = new FormGroup({
     column1: new FormControl()
   })
 
   getDataFormColumn2 = new FormGroup({
     column2: new FormControl()
   })
   
   //---------------------------------------//

  /*-----------Pattern discovery-----------*/
  //(*)--->
  public ASSRUL_DATA: MAssociationRules[] = [
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
    { antecedents: "0", consequents: "0", confidence: 0, support: 0},
  ];
  //--------------------------------------//

  @ViewChild(MatAccordion) accordionPD!: MatAccordion;
  @ViewChild('divRemPD') divRemPD!: ElementRef;

  @ViewChild('cotainerPrnt', { static: false }) containerPrnt!: ElementRef;
  @ViewChild('pageOne', { static: false }) pageOne!: ElementRef;
  @ViewChild('pageTwo', { static: false }) pageTwo!: ElementRef;
  @ViewChild('pageThre', { static: false }) pageThre!: ElementRef;

  public optionsMd = [
    { "models": "Anomalies" }, //<--- Cluster
    { "models": "Trends" }, //<---- Time Series
    { "models": "Frecuent patterns" }, // <--- Association rules
    //{ "models": "Decision rules" }, // <-- Decision tree
    //{ "models": "Outliers" }, // <-- Outliers
  ]

  public gphLinearTrd = {
    data: [
      { y: [0], x: [''], type: '' }, //<--- LINE CHART just add all the varibles with x and y values */
    ],
    layout: {
      title: "Tendency",
      yaxis: {
        title: "Rejected parts (percentage)",
        linecolor: "#333333",
      },
      //paper_bgcolor: '#F0F0F5',
      //plot_bgcolor:'#F0F0F5',
    },
    config: {responsive: false}
  };

  public gphLinearSer = {
    data: [
      { y: [0], x: [''], type: '' }, //<--- LINE CHART just add all the varibles with x and y values */
    ],
    layout: {
      title: "Serie",
      yaxis: {
        title: "Rejected parts (percentage)",
      },
      //paper_bgcolor: '#F0F0F5',
      //plot_bgcolor:'#F0F0F5',
    },
    config: {responsive: false}
  };

  public gphLinearSea = {
    data: [
      { y: [0], x: [''], type: '' }, //<--- LINE CHART just add all the varibles with x and y values */
    ],
    layout: {
      title: "Season",
      yaxis: {
        title: "Rejected parts (percentage)",
      },
      //paper_bgcolor: '#F0F0F5',
      //plot_bgcolor:'#F0F0F5',
    },
    config: {responsive: false}
  };

  public G = {
    data: [
      { x: [0], type: " " }, //<-- HISTOGRAM */
    ],

  };

  public graphScatter = {
    data: [
      {
        x: [''],
        y: [''],
        z: [0],
        mode: "markers",
        name: "Failed Nr",
        marker: {
          //color: "rgb(234, 153, 153)",
          color: [0],
          size: 12,
          symbol: "circle",
          line: {
            color: "rgb(204, 204, 204)",
            width: 1
          },
          opacity: 0.9
        },
        type: 'scatter3d',
        //type: 'scatter',
      }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
      {
        x: [''],
        y: [''],
        z: [''],
        mode: "markers",
        name: "centroids",
        marker: {
          color: "rgb(255, 217, 102)",
          //color: [0],
          size: 12,
          symbol: "circle",
          line: {
            color: "rgb(204, 204, 204)",
            width: 1
          },
          opacity: 0.9
        },
        //type: 'scatter3d',
        type: 'scatter3d',
      }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
    ],
    config: {responsive: false}
  };

  public scatterlayout = {
    title: "3D Cluster",
    yaxis: {
      title: "Operator",
    }
  };

  public graphHeatmap = {
    data: [{
      //x: [''],
      //y: [''],
      z: [0],
      type: 'heatmap'
    }],
    layout: {
      yaxis: {
        title:'Control'
      },
      xaxis:{
        title:'Control'
      },
      title: ''
    },
    config: {responsive: false}
  }

  public infoProjectPD!: any;
  private projects!: Array<Project>;
  public timeSeries!: Timeseries;

  public imagePath!: string;

  public isvisibleWelMCardPD: boolean = true;
  public isvisibleMCardPD: boolean = false;
  public panelOpenState: boolean = true;
  public panelStatePD: boolean = true;
  public edited1: boolean = false;
  public edited2: boolean = false;
  public edited3: boolean = false;
  public edited4: boolean = false;
  public edited: boolean = false;
  private isOpenedSTools: boolean = true;
  private isAnomalie!: string;
  public displayedColumsPD!: Array<string>;
  public smodelMessage!: string;
  public pdCompleted: boolean = false;
  public isDisabledPD: boolean = true;
  public pdRPCompleted!: boolean;
  public hasTable!: boolean;

  public clusterData!: any;
  public heatmapData:any=[];

  public optionsPr: ProjectsOp[] = [];
  public startDProject!: any;
  public endDProject!: any;
  public dataSource: any;
  public prTitle!: any;
  private prMDl!:any;

  private verPr!: boolean;
  private verMd!: boolean;

  //(2) Local storage code
  /*
  private location!: Location;
  private verPr:boolean = false;
  private verMd:boolean = false;
  private verStDt:boolean = false;
  private verEnDt:boolean = false;
  */

  getDataFormPD = new FormGroup({
    project: new FormControl(),
    models: new FormControl()
  })
  //---------------------------------------//

  ngOnInit(): void {

    //----------------Data overview----------------//
    this.api.getProjects("3", "prj_name").subscribe(dataP=>{
      this.optionsP=dataP;

      this.api.getProjects("3", "Operator").subscribe(dataO=>{
        this.optionsO=dataO;

        this.api.getProjects("3", "Status").subscribe(dataS=>{
          this.optionsS=dataS;

          this.getDataForm = new FormGroup({
            variable: new FormControl("project"),
            project: new FormControl(this.optionsP[1].prj_name),
            operator: new FormControl(this.optionsO[2].Operator),
            status: new FormControl(this.optionsS[1].Status),
            d_start: new FormControl(new Date().getFullYear()+"-01-01"),
            d_end: new FormControl(new Date().getFullYear()+"-12-31"),
            year: new FormControl("2020") //Revisar
          })

          this.completed = true;
          this.isDisabled = false;

        });

      });

    });

    this.getDataFormColumn1 = new FormGroup({
      column1: new FormControl("perfectControls")
    })

    this.getDataFormColumn2 = new FormGroup({
      column2: new FormControl("posPassCtrls")
    })

    //---------------------------------------------//

    /*--------------Pattern discovery--------------*/

    //-------->Pendiente (En BE)
    this.api.getprojectsPtD().subscribe(data=>{
      this.projects = data;
      this.projects.forEach(element => {
        const temp = { "projects": element.prj_name };
        this.optionsPr.push(temp);
      });

      this.getDataFormPD = new FormGroup({
        project: new FormControl(this.optionsPr[0].projects),
        models: new FormControl(this.optionsMd[0].models)
      })

      this.smodelMessage = this.optionsMd[0].models;
      this.pdCompleted = true; 
      this.isDisabledPD = false;
     //setTimeout(()=>{this.pdCompleted = true; this.isDisabledPD = false;},5000);

    })
    

  }

  ngAfterViewInit(): void {

    this.activatedRoute.queryParams.subscribe(params => {

      if(params.section == 'DataOverview'){

        var btnSlctTab: HTMLElement = document.getElementById('mat-tab-label-0-0') as HTMLElement;
        btnSlctTab.click();

        if(params.variable != undefined){

          this.verVari = false;
          //this.verValues = false;
          this.verstDateDO = false;
          this.verenDateDO = false;

            if(params.variable == "prj_name"){

              this.verVari = true;

            }else if(params.variable == "Operator"){

              this.verVari = true;

            }else if(params.variable == "Status"){

              this.verVari = true;

            }

            if(params.stDate == "2018"){
              this.verstDateDO = true;
            }else if(params.stDate == "2019"){
              this.verstDateDO = true;
            }else if(params.stDate == "2020"){
              this.verstDateDO = true;
            }

            if(params.enDate == "2018"){
              this.verenDateDO = true;
            }else if(params.enDate == "2019"){
              this.verenDateDO = true;
            }else if(params.enDate == "2020"){
              this.verenDateDO = true;
            }

          if(this.verVari == true && this.verstDateDO == true && this.verenDateDO == true){

            var cont = 0;

            if(params.variable == "prj_name"){

              this.api.getProjects("3", "prj_name").subscribe(dataP=>{

                this.datatemp = dataP
                this.datatemp.forEach((element: { prj_name: string, b_min:string, b_max:string, }) => {
                  if(element.prj_name == params.value){
                    this.isVariableVisible=1;
                    this.projectNameDO = params.value;
                    this.variable = params.variable;
                    this.value = params.value;
                    this.startDate = params.stDate+'-01-01';
                    this.endDate = params.enDate+'-12-31';
                    this.column1="perfectControls";
                    this.column2="posPassCtrls";
                    this.optionsP.forEach(element => {
                      if (element.prj_name == params.value) {
                        this.startProjectDate = element.b_min.toString();
                        this.endProjectDate = element.b_max.toString();
                      }
                    });
                    this.displayReport();
                    cont = 0;
                    return;
                  }
                  cont += 1;
                });
              });

            }else if(params.variable == "Operator"){

              this.api.getProjects("3", "Operator").subscribe(dataO=>{

                dataO.forEach((element: { Operator: string, b_min:string, b_max:string, }) => {
                  if(element.Operator == params.value){
                    this.isVariableVisible=2;
                    this.variable = params.variable;
                    this.value = params.value;
                    this.startDate = params.stDate+'-01-01';
                    this.endDate = params.enDate+'-12-31';
                    this.column1="perfectControls";
                    this.column2="posPassCtrls";
                    this.startProjectDate = element.b_min.toString();
                    this.endProjectDate = element.b_max.toString();
                    this.displayReport();
                    cont = 0;
                    return;
                  }
                });
                cont += 1;
              });

            }else if(params.variable == "Status"){

              this.api.getProjects("3", "Status").subscribe(dataS=>{

                dataS.forEach((element: { Status: string, b_min:string, b_max:string, }) => {
                  if(element.Status == params.value){
                    this.isVariableVisible=3;
                    this.variable = params.variable;
                    this.value = params.value;
                    this.startDate = params.stDate+'-01-01';
                    this.endDate = params.enDate+'-12-31';
                    this.column1="perfectControls";
                    this.column2="posPassCtrls";
                    this.startProjectDate = element.b_min.toString();
                    this.endProjectDate = element.b_max.toString();
                    this.displayReport();
                    cont = 0;
                    return;
                  }
                  cont += 1;
                });

              });

            }

            if(cont != 0){
              this.openErrorDialog('Broken link');
            }

          }else{
            this.openErrorDialog('Broken link');
          }


        } else if(params.variable == undefined){
          //hanlde other type of variables
        } else{
          this.openErrorDialog('Broken link');
        }


      }else if(params.section == 'PatternDiscovery'){

        var btnSlctTab: HTMLElement = document.getElementById('mat-tab-label-0-1') as HTMLElement;
        btnSlctTab.click();

        if(params.projectName != undefined){

          this.verPr = false;
          this.verMd = false;

          this.api.getprojectsPtD().subscribe(data => {
            this.projects = data;
            this.projects.forEach(element => {
              if(element.prj_name == params.projectName){
                this.verPr = true;
                this.selectPD(params.projectName)
                return;
              }
            });

            this.optionsMd.forEach(element => {
              if(element.models == params.discover){
                this.verMd = true;
                return;
              }
            });

            if(this.verPr == true && this.verMd == true){

              this.prTitle = params.projectName;
              this.prMDl = params.discover;
              this.displayReportPD(params.projectName, params.discover);

              // add the following code to the final version to put the default options
              // readed from the URI in the side-panel
              let indexPr = 0;
              let indexMd = 0;
              this.optionsPr.forEach((element,index) => {
                if(element.projects == params.projectName){
                  indexPr = index;
                }
              });

              this.optionsMd.forEach((element, index) => {
                if(element.models == params.discover){
                  indexMd = index
                }
              });

              this.getDataFormPD = new FormGroup({
                project: new FormControl(this.optionsPr[indexPr].projects),
                models: new FormControl(this.optionsMd[indexMd].models)
              })

              this.smodelMessage = this.optionsMd[indexMd].models;

            }else {
              this.openErrorDialog('Broken link')
            }

          });

        }

        }else if(params.section == undefined){
          //handle another section
        }else{
          this.openErrorDialog('Broken link')
        }

    });

    this.location.replaceState('dashboard')

  }

  //--------Data overview-------//

  onSubmit(form: any) {

    this.projectNameDO = form.project;

    switch (form.variable) {

      case "project":
        this.variable = "prj_name";
        this.value = form.project;
        break;

      case "operator":
        this.variable = "Operator";
        this.value = form.operator;
        
        break;
          
      case "status":
        this.variable = "Status";
        this.value = form.status;
        
        break;
    
      default:
        break;

    }

    this.column1="perfectControls"
    this.column2="posPassCtrls"
    
    //console.log(form.variable)
    //console.log(this.variable)
    //console.log(this.value)

    if(this.isPeriodVisible == 1){
      this.startDate = form.year + '-01-01';
      this.endDate = form.year + '-12-31';
      this.stYear = form.year;
      this.enYear = form.year;
    }
    else{
      this.startDate = form.d_start;
      this.endDate = form.d_end;
    }

    this.optionsP.forEach(element => {
      if (element.prj_name == form.project) {
        this.startProjectDate = element.b_min.toString()
        this.endProjectDate = element.b_max.toString()
      }
    });

    this.displayReport();
  }

  displayReport(){

    this.api.getDataOverview("1",this.variable,this.value,this.startDate,this.endDate).subscribe(data=>{
      this.dataOverview=data;
      this.tableData=data.sampleData;
      this.completed = true;
    });
    
    this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,this.column1).subscribe(dataGH1=>{
      this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,this.column2).subscribe(dataGH2=>{
        this.histogramC1(dataGH1, this.column1);
        this.histogramC2(dataGH2, this.column2);
      });
    });
    
    this.api.getTimeSeries("6",this.variable,this.value,this.startDate,this.endDate).subscribe(data=>{
      this.periodData=data;
      this.lineChart();
    });

    switch (this.variable) {

      case "prj_name":

        this.staticName = this.value;
        this.staticImage = this.projectNameDO;

        this.displayedColumns = [
          "CreationDate",
          "Status",
          "Operator",
          "controlNb",
          "perfectControls",
          "failedNb",
        ];
        
        this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,"Operator").subscribe(dataPC1=>{
          this.pieChartData1=dataPC1["categories"];

          this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,"Status").subscribe(dataPC2=>{
            this.pieChartData2=dataPC2["categories"];
            this.pieChart();
          });

        });

        this.api.getOperatorsCounts("7",this.variable,this.value,this.startDate,this.endDate).subscribe(data=>{
          this.operatorsCounts=data;
          this.histogram();
        });

        break;
        
      case "Operator":

        this.staticName = this.variable + ": " +this.value;
        this.staticImage = this.variable;

        this.displayedColumns = [
          "prj_name",
          "CreationDate",
          "Status",
          "controlNb",
          "perfectControls",
          "failedNb",
        ];

        this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,"prj_name").subscribe(dataPC1=>{
          this.pieChartData1=dataPC1["categories"];

          this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,"Status").subscribe(dataPC2=>{
            this.pieChartData2=dataPC2["categories"];
            this.pieChart();
          });

        });

        this.api.getOperatorsCounts("8",this.variable,this.value,this.startDate,this.endDate).subscribe(data=>{
          this.operatorsCounts=data;
          this.histogram();
        });

        break;
          
      case "Status":

        this.staticName = this.variable + ": " +this.value;
        this.staticImage = this.variable;

        this.displayedColumns = [
          "prj_name",
          "CreationDate",
          "Operator",
          "controlNb",
          "perfectControls",
          "failedNb",
        ];

        this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,"prj_name").subscribe(dataPC1=>{
          this.pieChartData1=dataPC1["categories"];

          this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,"Operator").subscribe(dataPC2=>{
            this.pieChartData2=dataPC2["categories"];
            this.pieChart();
          });

        });

        this.api.getOperatorsCounts("9",this.variable,this.value,this.startDate,this.endDate).subscribe(data=>{
          this.operatorsCounts=data;
          this.histogram();
        });

        break;
    
      default:
        break;
    }

    if(this.isvisibleWelMCard == true){
      this.closeWelcomM()
    }

    this.isvisibleMCard = true;

    setTimeout(()=>{this.completed2 = true;},5000);

  }

  pieChart() {
    
    switch (this.variable) {
      
      case "prj_name":

        this.graphPie1 = {
          data: [
            { values: this.pieChartData1.counts, labels: this.pieChartData1.Operator, type: 'pie' },
          ],
          layout: {title: 'Pieces measured by operator'},
          config: {responsive: false}
        };

        this.graphPie2 = {
          data: [
            { values: this.pieChartData2.counts, labels: this.pieChartData2.Status, type: 'pie' },
          ],
          layout: {title: 'Approved / Rejected pieces'},
          config: {responsive: false}
        };
        
        break;
        
      case "Operator":
        
        this.graphPie1 = {
          data: [
            { values: this.pieChartData1.counts, labels: this.pieChartData1.prj_name, type: 'pie' },
          ],
          layout: {title: 'Pieces measured by project'},
          config: {responsive: false}
        };

        this.graphPie2 = {
          data: [
            { values: this.pieChartData2.counts, labels: this.pieChartData2.Status, type: 'pie' },
          ],
          layout: {title: 'Approved / Rejected pieces'},
          config: {responsive: false}
        };
        
      break;
          
      case "Status":
        this.graphPie1 = {
          data: [
            { values: this.pieChartData1.counts, labels: this.pieChartData1.prj_name, type: 'pie' },
          ],
          layout: {title: this.value + ' pieces by project'},
          config: {responsive: false}
        };

        this.graphPie2 = {
          data: [
            { values: this.pieChartData2.counts, labels: this.pieChartData2.Operator, type: 'pie' },
          ],
          layout: {title: this.value + ' pieces by operator'},
          config: {responsive: false}
        };
            
        break;
    
      default:
        break;
    }

  }

  histogramC1(data: any, column: any){

    switch (column) {
      case "perfectControls":
        this.histogramName1 = "% Perfect controls";
        break;

      case "posPassCtrls":
        this.histogramName1 = "% Passed controls +";
        break;

      case "negPassCtrls":
        this.histogramName1 = "% Passed controls -";
        break;

      case "posFailCtrls":
        this.histogramName1 = "% Failed controls +";
        break;

      case "negFailCtrls":
        this.histogramName1 = "% Failed controls -";
        break;

      case "absDeviation":
        this.histogramName1 = "Average deviation";
        break;

      case "maxDevPos":
        this.histogramName1 = "Maximum deviation +";
        break;

      case "maxDevNeg":
        this.histogramName1 = "Maximum deviation -";
        break;

      case "passNb":
        this.histogramName1 = "Passed controls";
        break;

      case "failedNb":
        this.histogramName1 = "Failed controls";
        break;

      case "day":
        this.histogramName1 = "Day: Day";
        break;

      case "hour":
        this.histogramName1 = "Hour: Time";
        break;

      default:
        break;
    }

    this.graphHistogramG1 = {
      data: [
        {  x: data.values, marker: { color: "", }, type: 'histogram' }
      ],
      layout: { title: this.histogramName1},
      config: {responsive: false}
    };
  }

  histogramC2(data: any, column: any){

    switch (column) {
      case "perfectControls":
        this.histogramName2 = "% Perfect controls";
        break;

      case "posPassCtrls":
        this.histogramName2 = "% Passed controls +";
        break;

      case "negPassCtrls":
        this.histogramName2 = "% Passed controls -";
        break;

      case "posFailCtrls":
        this.histogramName2 = "% Failed controls +";
        break;

      case "negFailCtrls":
        this.histogramName2 = "% Failed controls -";
        break;

      case "absDeviation":
        this.histogramName2 = "Average deviation";
        break;

      case "maxDevPos":
        this.histogramName2 = "Maximum deviation +";
        break;

      case "maxDevNeg":
        this.histogramName2 = "Maximum deviation -";
        break;

      case "passNb":
        this.histogramName2 = "Passed controls";
        break;

      case "failedNb":
        this.histogramName2 = "Failed controls";
        break;

      case "day":
        this.histogramName2 = "Day: Day";
        break;

      case "hour":
        this.histogramName2 = "Hour: Time";
        break;

      default:
        break;
    }

    this.graphHistogramG2 = {
      data: [
        {  x: data.values, marker: { color: "", }, type: 'histogram' }
      ],
      layout: { title: this.histogramName2 },
      config: {responsive: false}
    };

  }

  histogram(){

    switch (this.variable) {
      
      case "prj_name":
        this.graph = {
          data: [
            { x: this.operatorsCounts["Operator"], y: this.operatorsCounts["Approved"], name: 'Approved', marker: { color: "rgb(144, 238, 144)"}, type: 'bar' },
            { x: this.operatorsCounts["Operator"], y: this.operatorsCounts["Rejected"], name: 'Rejected', marker: { color: "rgb(255, 0, 0)"},type: 'bar' }
          ],
          layout: { title: 'Approved / Rejected pieces by operator'},
          config: {responsive: false}
        };

        break;
        
      case "Operator":
        
        this.graph = {
          data: [
            { x: this.operatorsCounts["prj_name"], y: this.operatorsCounts["Approved"], name: 'Approved', marker: { color: "rgb(144, 238, 144)"}, type: 'bar' },
            { x: this.operatorsCounts["prj_name"], y: this.operatorsCounts["Rejected"], name: 'Rejected', marker: { color: "rgb(255, 0, 0)"},type: 'bar' }
          ],
          layout: { title: 'Approved / Rejected pieces by project'},
          config: {responsive: false}
        };
        break;
      
      case "Status":

        this.graph = {
          data: [
            { x: this.operatorsCounts["Operator"], y: this.operatorsCounts["CONFIDENTIAL 65720 TVA KTH"], name: 'CONFIDENTIAL 65720 TVA KTH', marker: { color: ""}, type: 'bar' },
            { x: this.operatorsCounts["Operator"], y: this.operatorsCounts["CONFIDENTIAL R60810 TVA_TWA Frt Side Frame"], name: 'CONFIDENTIAL R60810 TVA_TWA Frt Side Frame', marker: { color: ""},type: 'bar' }
          ],
          layout: { title: 'Pieces mesured by operator'},
          config: {responsive: false}
        };

        break;
    
      default:
        break;
    }

  }

  lineChart(){

    switch (this.variable) {
      
      case "prj_name":

        this.graph3 = {
          data: [
            {x: this.periodData.index, y: this.periodData.serie, name: 'Approved', marker: { color: ''}, type: 'scatter'},
            {x: this.periodData.index, y: this.periodData.serie_f, name: 'Rejected', marker: { color: "rgb(255, 0, 0)"}, type: 'scatter'}
          ],
          layout: {
              title: 'Approved / Rejected pieces per day',
              yaxis: {
                title: "Total count",
              }
            },
          config: {responsive: false}
        };

        break;
        
      case "Operator":

        this.graph3 = {
          data: [
            {x: this.periodData.index, y: this.periodData.serie, name: 'Approved', marker: { color: ''}, type: 'scatter'},
            {x: this.periodData.index, y: this.periodData.serie_f, name: 'Rejected', marker: { color: "rgb(255, 0, 0)"}, type: 'scatter'}
          ],
          layout: {
              title: 'Approved / Rejected pieces per day',
              yaxis: {
                title: "Total count",
              }
            },
          config: {responsive: false}
        };
        
        break;
      
      case "Status":

        if (this.value=="Approved") {
          this.graph3 = {
            data: [
              {x: this.periodData.index, y: this.periodData.serie, name: 'Approved', marker: { color: ''}, type: 'scatter'}
            ],
            layout: {
                title: this.value +' pieces per day',
                yaxis: {
                  title: "Total count",
                }
              },
            config: {responsive: false}
          };
        }

        if (this.value=="Rejected") {
          this.graph3 = {
            data: [
              {x: this.periodData.index, y: this.periodData.serie_f, name: 'Rejected', marker: { color: "rgb(255, 0, 0)"}, type: 'scatter'}
            ],
            layout: {
                title: this.value +' pieces per day',
                yaxis: {
                  title: "Total count",
                }
              },
            config: {responsive: false}
          };
        }

        break;
    
      default:
        break;
    }

  }

  select(option: any){

    switch (option) {
      case "project":
        this.isVariableVisible=1;
        break;

      case "operator":
        this.isVariableVisible=2;
        break;

      case "status":
        this.isVariableVisible=3;
        break;
    
      default:
        console.log("Error")
        break;
    }

    //this.isvisibleMCard = false;

  }

  onItemChange(i: any) {
    this.isPeriodVisible = i;
    console.log(i)
  }

  onChangeColumn1(form: any){
    this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,form.column1).subscribe(data=>{
      this.histogramC1(data, form.column1)
    });
    
  }

  onChangeColumn2(form: any){
    this.api.getSecondEndpoint("2",this.variable,this.value,this.startDate,this.endDate,form.column2).subscribe(data=>{
      this.histogramC2(data, form.column2)
    });
  }

  prueba(tab: any){
    console.log(tab)
  }

  savePDF():void{
    
    html2canvas(this.imprimeDO.nativeElement).then( (canvas) => {

      var imgHeight = canvas.height * 270 / canvas.height; //<--- 208 / canvas.width

      var imgData = canvas.toDataURL('image/jpeg');
      var doc = new jsPDF("p","mm", "letter");

      doc.addImage(imgData, 4, 5, 208, imgHeight);

      html2canvas(this.imprimeCOD.nativeElement).then( (canvas) => {

        doc.addPage();

        var imgHeight = canvas.height * 270 / canvas.height; //<--- 208 / canvas.width
    
        var imgData = canvas.toDataURL('image/jpeg');
    
        doc.addImage(imgData, 4, 5, 208, imgHeight);

        html2canvas(this.imprimeCOT.nativeElement).then( (canvas) => {

          doc.addPage();

          var imgHeight = canvas.height * 270 / canvas.width; //<--- 208 / canvas.width
      
          var imgData = canvas.toDataURL('image/jpeg');
      
          doc.addImage(imgData, 4, 5, 208, imgHeight);

          var now = new Date();
          var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
          doc.save(this.variable+''+this.value+'.pdf');  
      
        });
    
      });

    });

    

  }

  copyToClipboardDO():void {

    //console.log(this.variable)
    //console.log(this.value)
    //console.log(this.startDate)
    //console.log(this.endDate)

    let section = 'dashboard' + encodeURI('?section=DataOverview'+'&variable='+this.variable+'&value='+
                                          this.value+'&stDate='+this.stYear+'&enDate='+this.enYear);
    this.location.replaceState('dashboard')
    this.location.replaceState(section)

    let URLtext = 'http://192.168.50.75'+this.location.path();
    this.clipboard.copy(URLtext) ;
    this.openErrorDialog('Link copied to clipboard!');

  }

  closeWelcomM(){
    this.divRem.nativeElement.remove();
    this.isvisibleWelMCard = false;
  }

  closeReportWdgt(){
    this.isvisibleMCard = false;
  }

  openColumnDialog(){
    this.columnD.open(ColumnDialog);
  }
  //----------------------------//


  /*-----Pattern discovery-----*/

  onSubmitPD(form: any) {
    this.prTitle = form.project;
    this.prMDl = form.models;
    this.displayReportPD(form.project, form.models);

    this.projects.forEach(element => {
      if (element.prj_name == form.project) {
        this.startDProject = element.b_min.toString()
        this.endDProject = element.b_max.toString()
      }
    });

    /*------------------Pendiente------------------*/

    /*this.api.getDataOverview(this.startDProject, this.endDProject, form.project, "1").subscribe(data => {

      this.infoProjectPD = data;

    });*/
  }

  selectPD(option: any){

    switch (option) {

      case "CONFIDENTIAL R60810 TVA_TWA Frt Side Frame":
        this.optionsMd = [
          { "models": "Anomalies" }, //<--- Cluster
          { "models": "Trends" }, //<---- Time Series
          { "models": "Frecuent patterns" }, // <--- Association rules
          { "models": "Decision rules" }, // <-- Decision tree
          //{ "models": "Outliers" }, // <-- Outliers
        ]
        break;

      default:
        this.optionsMd = [
          { "models": "Anomalies" }, //<--- Cluster
          { "models": "Trends" }, //<---- Time Series
          { "models": "Frecuent patterns" }, // <--- Association rules
          //{ "models": "Decision rules" }, // <-- Decision tree
          //{ "models": "Outliers" }, // <-- Outliers
        ]
        this.getDataFormPD = new FormGroup({
          project: new FormControl(this.optionsPr[0].projects),
          models: new FormControl(this.optionsMd[0].models)
        })
        break;
    }

  }

  selectMDPD(option: any){
    this.smodelMessage = option;
  }

  displayReportPD(project: string, model: string) {

    switch (model) {

      //clustering
      case "Anomalies": {

        this.pdRPCompleted = false;
        //the following variable is used in the fuction savePDF
        this.isAnomalie = model;

        if (this.isvisibleWelMCardPD == true) {
          this.closeWelcomMPD()
        }

        this.hasTable = false;
        this.edited = false;
        this.edited2 = true;
        this.edited3 = false;
        this.edited1 = false;
        this.edited4 = false;
        this.isvisibleMCardPD = true;

        this.imagePath = "/assets/img/" + project;

        this.displayedColumsPD = ['Antecedents', 'Consequents', 'Confidence', 'Support'];

        this.dataSource = new MatTableDataSource<MAssociationRules>(this.ASSRUL_DATA);

        this.api.getClusterC(project).subscribe(data => {

          this.infoProjectPD = data;
          this.clusterData = data;

          if(this.prTitle == 'CONFIDENTIAL 65720 TVA KTH'){

             //cluster
            this.graphScatter = {
              data: [
                {
                  x: this.clusterData.x,
                  y: this.clusterData.y,
                  z: this.clusterData.z,
                  mode: "markers",
                  name: "Failed Nr",
                  marker: {
                    //color: "rgb(234, 153, 153)",
                    color: this.clusterData.clusters,
                    size: 12,
                    symbol: "circle",
                    line: {
                      color: "rgb(204, 204, 204)",
                      width: 1
                    },
                    opacity: 0.9
                  },
                  type: 'scatter3d',
                  //type: 'scatter',
                }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
                {
                  x: [ 
                    dateFormat(this.clusterData.centroids[0][5].toString(), "yyyy-mm-dd"),
                    dateFormat(this.clusterData.centroids[1][5].toString(), "yyyy-mm-dd"),
                    dateFormat(this.clusterData.centroids[2][5].toString(), "yyyy-mm-dd"),
                    dateFormat(this.clusterData.centroids[3][5].toString(), "yyyy-mm-dd"),
                  ],

                  y: [ 
                    this.clusterData.centroids[0][7],
                    this.clusterData.centroids[1][7],
                    this.clusterData.centroids[2][7],
                    this.clusterData.centroids[3][7]
                  ],

                  z: [ 
                    this.clusterData.centroids[0][1],
                    this.clusterData.centroids[1][1],
                    this.clusterData.centroids[2][1],
                    this.clusterData.centroids[3][1],
                  ],

                  mode: "markers",
                  name: "centroids",
                  marker: {
                    color: "rgb(255, 217, 102)",
                    //color: [0],
                    size: 12,
                    symbol: "circle",
                    line: {
                      color: "rgb(204, 204, 204)",
                      width: 1
                    },
                    opacity: 0.9
                  },
                  type: 'scatter3d',
                  //type: 'scatter',
                }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
              ],
              config: {responsive: false}
            };

          }else{

            this.hasTable = true;
             //cluster
            this.graphScatter = {
              data: [
                {
                  x: this.clusterData.x,
                  y: this.clusterData.y,
                  z: this.clusterData.z,
                  mode: "markers",
                  name: "Failed Nr",
                  marker: {
                    //color: "rgb(234, 153, 153)",
                    color: this.clusterData.clusters,
                    size: 12,
                    symbol: "circle",
                    line: {
                      color: "rgb(204, 204, 204)",
                      width: 1
                    },
                    opacity: 0.9
                  },
                  type: 'scatter3d',
                  //type: 'scatter',
                }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
                {
                  x: [ 
                    dateFormat(this.clusterData.centroids[0][5].toString(), "yyyy-mm-dd"),
                    dateFormat(this.clusterData.centroids[1][5].toString(), "yyyy-mm-dd"),
                    dateFormat(this.clusterData.centroids[2][5].toString(), "yyyy-mm-dd"),
                  ],

                  y: [ 
                    this.clusterData.centroids[0][7],
                    this.clusterData.centroids[1][7],
                    this.clusterData.centroids[2][7],
                  ],

                  z: [ 
                    this.clusterData.centroids[0][1],
                    this.clusterData.centroids[1][1],
                    this.clusterData.centroids[2][1],
                  ],

                  mode: "markers",
                  name: "centroids",
                  marker: {
                    color: "rgb(255, 217, 102)",
                    //color: [0],
                    size: 12,
                    symbol: "circle",
                    line: {
                      color: "rgb(204, 204, 204)",
                      width: 1
                    },
                    opacity: 0.9
                  },
                  type: 'scatter3d',
                  //type: 'scatter',
                }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
              ],
              config: {responsive: false}
            };

          }

          //console.log(data)
          //console.log(dateFormat(data.centroids[0][5].toString(), "yyyy-mm-dd"))
         
          this.pdRPCompleted = true;
          //setTimeout(()=>{},5000);

        });

        break;

      }

      //time series
      case "Trends": {

        this.pdRPCompleted = false;
        //the following variable is used in the fuction savePDF
        this.isAnomalie = model;

        if (this.isvisibleWelMCardPD == true) {
          this.closeWelcomMPD()
        }

        this.edited = true;
        this.edited3 = false;
        this.edited2 = false;
        this.edited1 = false;
        this.edited4 = false;
        this.isvisibleMCardPD = true;

        this.imagePath = "/assets/img/" + project;

        this.api.getTSeriesC(project).subscribe(data => {
          this.infoProjectPD = data;
          this.timeSeries = data;

          //trend
          this.gphLinearTrd = {
            data: [
              { y: this.timeSeries.trend, x: this.timeSeries.index, type: 'scatter' }, //<--- LINE CHART just add all the varibles with x and y values */
            ],
            layout: {
              title: "Tendency",
              yaxis: {
                title: "Rejected parts (percentage)",
                linecolor: "#333333",
              },
              //paper_bgcolor: '#F0F0F5',
              //plot_bgcolor:'#F0F0F5',
            },
            config: {responsive: false}
          };

          //Serie
          this.gphLinearSer = {
            data: [
              { y: this.timeSeries.serie, x: this.timeSeries.index, type: 'scatter' }, //<--- LINE CHART just add all the varibles with x and y values */
            ],
            layout: {
              title: "Serie",
              yaxis: {
                title: "Rejected parts (percentage)",
              },
              //paper_bgcolor: '#F0F0F5',
              //plot_bgcolor:'#F0F0F5',
            },
            config: {responsive: false}
          };

          //Season
          this.gphLinearSea = {
            data: [
              { y: this.timeSeries.seasonality, x: this.timeSeries.index, type: 'scatter' }, //<--- LINE CHART just add all the varibles with x and y values */
            ],
            layout: {
              title: "Season",
              yaxis: {
                title: "Rejected parts (percentage)",
              },
              //paper_bgcolor: '#F0F0F5',
              //plot_bgcolor:'#F0F0F5',
            },
            config: {responsive: false}
          };
          this.pdRPCompleted = true;
          //setTimeout(()=>{this.pdRPCompleted = true;},5000);

        });

        break;

      }

      //Association Rules - A priori algorithm
      case "Frecuent patterns": {

        this.pdRPCompleted = false;
        //the following variable is used in the fuction savePDF
        this.isAnomalie = model;

        if (this.isvisibleWelMCardPD == true) {
          this.closeWelcomMPD()
        }

        this.edited = false;
        this.edited3 = true;
        this.edited2 = false;
        this.edited1 = false;
        this.edited4 = false;
        this.isvisibleMCardPD = true;

        this.imagePath = "/assets/img/" + project;

        this.api.getAssociationR(project).subscribe( (dataAssR) => {
          this.infoProjectPD = dataAssR;
          this.displayedColumsPD = ['Antecedents', 'Consequents', 'Confidence', 'Support'];
          this.dataSource = new MatTableDataSource<MAssociationRules>(this.infoProjectPD.model);

          this.pdRPCompleted = true;
          //setTimeout(()=>{this.pdRPCompleted = true;},5000);
        });

        this.api.getHeatmapC(project).subscribe( (dataH)=>{
          this.heatmapData = dataH
          this.graphHeatmap = {
            data: [
              {
                //x: this.heatmapData.index,
                //y: this.heatmapData.columns,
                z: this.heatmapData.data,
                type: 'heatmap'
              }
            ],
            layout: {
              yaxis: {
                title:'Control'
              },
              xaxis:{
                title:'Control'
              },
              title: 'Heatmap'
            },
            config: {responsive: false}
          };
        });


        break;

      }

      //decision tree
      case "Decision rules": {

        this.pdRPCompleted = false;
        //the following variable is used in the fuction savePDF
        this.isAnomalie = model;

        if(this.isvisibleWelMCardPD == true){
          this.closeWelcomMPD()
        }
        this.edited4 = true;
        this.edited3 = false;
        this.edited2 = false;
        this.edited1 = false;
        this.edited = false;
        this.isvisibleMCardPD = true;

        this.imagePath = "/assets/img/CONFIDENTIAL R60810 TVA_TWA Frt Side Frame";

        this.api.getDecisionR().subscribe((dataDR) => {
          this.infoProjectPD = dataDR;
          this.pdRPCompleted = true;
          //setTimeout(()=>{this.pdRPCompleted = true;},5000);
        });

        break;

      }

      /*outlier algorithm
      case "Outliers": {

        //the following variable is used in the fuction savePDF
        this.isAnomalie = model;

        if(this.isvisibleWelMCardPD == true){
          this.closeWelcomMPD()
        }
        this.edited3 = true;
        this.edited2 = false;
        this.edited1 = false;
        this.edited = false;
        this.isvisibleMCardPD = true;

        this.imagePath = "/assets/img/"+project;

        break;

      }*/

      default: {
        console.log("error");
        break;

      }

    }

  }

  openModelDialog(): void {
    this.modelD.open(DialogOverviewModelDialog, {data: {smodel: this.smodelMessage}});

    /*
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
    */
  }

  openErrorDialog(message: string): void {
    this.modelED.open(ErrorDialogModel, {width: '280.9px', data: {message: message}});
  }

  copyToClipboard(): void{

    let section = 'dashboard' + encodeURI('?section=PatternDiscovery'+'&projectName='+this.prTitle+'&discover='+this.prMDl)
    this.location.replaceState('dashboard')
    this.location.replaceState(section)

    let URLtext = 'http://192.168.50.75'+this.location.path()
    this.clipboard.copy(URLtext)
    this.openErrorDialog('Link copied to clipboard!')
  }

  closeWelcomMPD(){
    this.divRemPD.nativeElement.remove();
    this.isvisibleWelMCardPD = false;
  }

  closeReportWdgtPD(){
    this.isvisibleMCardPD = false;
  }

  isOpndTools(){
    this.isOpenedSTools = !this.isOpenedSTools;
  }

  pdsavePDF() {

    this.openErrorDialog("Downloading");
    /*var btnOpnTools: HTMLElement = document.getElementById('openPanePD') as HTMLElement;
    if(this.isOpenedSTools == false){
      btnOpnTools.click();
    }*/

    if( this.platform.SAFARI == true ){

      //can be a function
      html2canvas(this.pageOne.nativeElement).then((canvas) => {
        //console.log(canvas.getContext('webgl2', { preserveDrawingBuffer:true }))
        var imgWidth = 210;
        var pageHeight = 295;
        var imgHeight = canvas.height * imgWidth / canvas.width;
        var heightLeft = imgHeight;

        var imgData = canvas.toDataURL('image/jpeg');
        var doc = new jsPDF("p","mm", "letter");
        var position = 0

        doc.addImage(imgData, 3, 2, imgWidth, imgHeight+10);
        heightLeft -= pageHeight;

        while (heightLeft >= 0) {
          position = heightLeft - imgHeight;
          doc.addPage();
          doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight + 10);
          heightLeft -= pageHeight;
        }
        //const imgProps= doc.getImageProperties(imgData);
        //var width = doc.internal.pageSize.getWidth();
        //var heigh = (imgProps.height * width) / imgProps.width;
        //var imgHeigth = (canvas.height * 208) / canvas.width;

        var now = new Date();
        var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
        doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');

      });

    }else{

      if(this.isAnomalie == 'Anomalies'){

        //console.log('impresion')

        html2canvas(this.pageOne.nativeElement).then((canvas) => {

          var imgHeigth = (canvas.height * 270) / canvas.height; //<--- 208 / canvas.width

          var imgData = canvas.toDataURL('image/jpeg');
          var doc = new jsPDF("p","mm", "letter");

          doc.addImage(imgData, 4, 5, 208, imgHeigth);

          if(this.prTitle == 'CONFIDENTIAL 65720 TVA KTH'){

            html2canvas(this.pageTwo.nativeElement).then((canvasT) => {

              doc.addPage();
              var imgHeightT = canvasT.height * 270 / canvasT.width; //<--- 208 / canvas.width
  
              var imgDataT = canvasT.toDataURL('image/jpeg');
  
              doc.addImage(imgDataT, 4, 5, 208, imgHeightT);
  
              var graphDiv = this.plotlyService.getInstanceByDivId('graficaImprimir')
  
              this.plotlyService.getPlotly().then((canvaD) => {
  
  
                  var imgDataD = canvaD.toImage(graphDiv, {format: 'png', width: 800, height: 600})//, {format: 'png', width: 1024, height: 800, filename: 'test' });
                  imgDataD.then((dataURL: any) =>{
  
                    doc.addImage(dataURL, 8, 55, 200, 120);//180, 135);
  
                    var now = new Date();
                    var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
                    doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');
                  });

              });
  
            });

          }else{

            html2canvas(this.pageTwo.nativeElement).then((canvasT) => {

              doc.addPage();
              var imgHeightT = canvasT.height * 270 / canvasT.height; //<--- 208 / canvas.width
  
              var imgDataT = canvasT.toDataURL('image/jpeg');
  
              doc.addImage(imgDataT, 4, 5, 208, imgHeightT);
  
              var graphDiv = this.plotlyService.getInstanceByDivId('graficaImprimir')
  
              this.plotlyService.getPlotly().then((canvaD) => {
  
  
                var imgDataD = canvaD.toImage(graphDiv, {format: 'png', width: 800, height: 600})//, {format: 'png', width: 1024, height: 800, filename: 'test' });
                imgDataD.then((dataURL: any) =>{
  
                doc.addImage(dataURL, 8, 55, 200, 110);//180, 135);
  
                var now = new Date();
                var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
                doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');
                });

              });
  
            });

          }

          

        });


        /*const graphDiv = this.plotlyService.getInstanceByDivId('graficaImprimir')
        this.plotlyService.getPlotly().then((canva) => {
          var imgData = canva.toImage(graphDiv, {format: 'png', width: 800, height: 600})//, {format: 'png', width: 1024, height: 800, filename: 'test' });
          //console.log(imgData)
          imgData.then((dataURL: any) =>{
            //console.log(dataURL)
            //const doc = new jsPDF("p", "mm", "a4");
            doc.addImage(dataURL, 0, 0, 208, 200)
            doc.save('demo.pdf')
          })
        })*/

      }else { //<--- can be a function

        html2canvas(this.pageOne.nativeElement).then((canvas) => {
          //console.log(canvas.getContext('webgl2', { preserveDrawingBuffer:true }))
          //var imgWidth = 210;
          //var pageHeight = 295;
          var imgHeight = canvas.height * 270 / canvas.height; //<--- 208 / canvas.width
          //var heightLeft = imgHeight;

          var imgData = canvas.toDataURL('image/jpeg');
          var doc = new jsPDF("p","mm", "letter");
          //var position = 3

          doc.addImage(imgData, 4, 5, 208, imgHeight);
          //heightLeft -= pageHeight;
          //heightLeft + 12;

          /*while (heightLeft >= 0) {
            position = heightLeft - imgHeight;
            doc.addPage();
            doc.addImage(imgData, 3, position, imgWidth, imgHeight + 10);
            heightLeft -= pageHeight;
            heightLeft += 12;
          }*/
          //const imgProps= doc.getImageProperties(imgData);
          //var width = doc.internal.pageSize.getWidth();
          //var heigh = (imgProps.height * width) / imgProps.width;
          //var imgHeigth = (canvas.height * 208) / canvas.width;

          html2canvas(this.pageTwo.nativeElement).then((canvasT) => {

            doc.addPage();
            var band = false;

            if(this.isAnomalie == 'Decision rules'){

              var imgHeightT = canvasT.height * 270 / canvasT.width; //<--- 208 / canvas.width
              var imgDataT = canvasT.toDataURL('image/jpeg');

              doc.addImage(imgDataT, 4, 5, 208, imgHeightT);

              var now = new Date();
              var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
              doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');

              band = true;

            }

            var imgHeightT = canvasT.height * 270 / canvasT.height; //<--- 208 / canvas.width

            var imgDataT = canvasT.toDataURL('image/jpeg');

            doc.addImage(imgDataT, 4, 5, 208, imgHeightT);

            if(this.isAnomalie == 'Frecuent patterns'){

              var graphDiv = this.plotlyService.getInstanceByDivId('heatmapGraph')
              this.plotlyService.getPlotly().then((canvaD) => {

                var imgDataD = canvaD.toImage(graphDiv, {format: 'png', width: 800, height: 600})//, {format: 'png', width: 1024, height: 800, filename: 'test' });
                imgDataD.then((dataURL: any) =>{
                doc.addImage(dataURL, 7, 40, 200, 90);//180, 135);

                var now = new Date();
                var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
                doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');

                });

              });

            }else if(band == false){

              html2canvas(this.pageThre.nativeElement).then((canvasE) => {

                doc.addPage();
                var imgHeightE = canvasE.height * 250 / canvasE.width; //<--- 208 / canvas.width

                var imgDataE = canvasE.toDataURL('image/png');

                doc.addImage(imgDataE, 4, 5, 208, imgHeightE);

                var now = new Date();
                var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
                doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');

              });

            }



          });

        });

      }

    }

  }

  //---------------------------//

}

//-------------Data Overview-------------//
@Component({
  selector: 'histogramInfo-dashboard',
  templateUrl: './histogramInfo-dashboard.html',
  styleUrls: ['./histogramInfo-dashboard.css']
})

export class ColumnDialog {
  
  constructor(public modelDRef: MatDialogRef<ColumnDialog>,
    @Inject(MAT_DIALOG_DATA) public data:DialogMData) { }
  
  onNoClick(): void {
    this.modelDRef.close();
  }
}

//---------------------------------------//

//-----------Pattern discovery-----------//
@Component({
  selector: 'modelD-dashboard',
  templateUrl: './modelD-dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DialogOverviewModelDialog {

  constructor(public modelDRef: MatDialogRef<DialogOverviewModelDialog>,
    @Inject(MAT_DIALOG_DATA) public data:DialogMData) { }

  onNoClick(): void {
    this.modelDRef.close();
  }
}

@Component({
  selector: 'errordata-dashboard',
  templateUrl: './errordata-dashboard.component.html',
  styleUrls: ['./errordashboard.component.css']
})

export class ErrorDialogModel {

  constructor(public modelEDRef: MatDialogRef<ErrorDialogModel>,
    @Inject(MAT_DIALOG_DATA) public data:DialogData) { }

  onNoClick(): void {
    this.modelEDRef.close();
  }
}
//---------------------------------------//
