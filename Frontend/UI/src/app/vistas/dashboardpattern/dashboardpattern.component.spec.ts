import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardpatternComponent } from './dashboardpattern.component';

describe('DashboardpatternComponent', () => {
  let component: DashboardpatternComponent;
  let fixture: ComponentFixture<DashboardpatternComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardpatternComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardpatternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
