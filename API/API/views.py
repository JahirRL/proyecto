from django.utils.decorators import method_decorator
from django.views import View
from .models import Pieces
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# initialize singleton
singleton = Pieces()
print(singleton.pieces.info())


# Create your views here.
@method_decorator(csrf_exempt, name='dispatch')
class MyCollectionView(View):
    def get(self, request):
        # data = json.loads(request.body.decode("utf-8"))
        coll = Pieces()
        # df = coll.getAllPieces()
        # df_ret = coll.getPiecesByDateRange("2018-10-01", "2018-10-10")

        return JsonResponse({"status": "ok"}, safe=False)


@csrf_exempt
def describeDataset(request):
    """
    Describe variable and obtain a data structure to plot.
    :param request: Post variable with:
        - type (type of description {1: All dataset, 2: one variable in dataset}
                                    [both cases consider date range and project])
        - d_start (date range starts)
        - d_end (date range ends)
        - project (name of project)
        - variable (variable to describe)
    """
    response = {"status": 'FAIL', 'message': 'se requiere una consulta tipo GET'}
    if request.GET:
        req = request.GET
        type_r = req['type']
        if type_r == "3":  # projects
            response = singleton.getProjects()
        else:
            d_start = req['d_start']
            d_end = req['d_end']
            project = req['project']
            if type_r == "2":  # Info variable
                variable = req['variable']
                response = singleton.getVariableDescr(project, d_start, d_end, variable)
            elif type_r == "1":  # info dataset
                response = singleton.getDescripByProject(project, d_start, d_end)
            elif type_r == "4":
                variable = req['variable']
                response = singleton.compareQC(project, d_start, d_end, variable)
            elif type_r == "5":
                variable = req['variable']
                response = singleton.DispVar(project, d_start, d_end, variable)
            elif type_r == '6': # time series
                variable = req['variable']
                value = req['value']
                response = singleton.getTimeSeries(variable, value, d_start, d_end)
            elif type_r == '7': # Counts
                response = singleton.getCountsByOperator(project, d_start, d_end)
            else:
                response["message"] = 'No se encontró una solicitud válida'
    return JsonResponse(response, safe=False)

def getModelResult(request):
    response = {"status": 'FAIL', 'message': 'se requiere una consulta tipo GET'}
    if request.GET:
        req = request.GET
        model = req['model']
        project = req['project']
        if model == '1':  # clustering
            response = singleton.kPrototypeClustering(project)
        elif model == '2':  # time series
            response = singleton.timeSerieAnalysis(project)
        elif model == '3':  # heatmap
            response = singleton.getHeatmapD(project)
        else:
            response["message"] = 'no existe un modelo con el identificador proporcionado'
    return JsonResponse(response, safe=False)

    
@csrf_exempt
def exampleEp(request):
    """
    example endpoint to test some new functions
    :param request: get request
    """
    response = {"status": 'FAIL', 'message': 'se requiere una consulta tipo POST'}
    if request.POST:
        coll = Pieces()
        # response = coll.getDatabaseByAgg('CONFIDENTIAL 65720 TVA KTH','2018-10-01', '2018-10-10')
    return JsonResponse({"status": "ok"}, safe=False)
