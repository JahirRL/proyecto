export interface Infoproject {
    percntMissing: number,​
    cells: number,
    missing: number,
    missingXatribute: Array<any>,
    observations: number,
    sampleData: Array<any>,
    variables: number,
}
