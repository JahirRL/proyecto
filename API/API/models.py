from pymongo import MongoClient
from .utils import Util
from .analysis import *
import pandas as pd

"""import pprint
import json
"""


# Create your models here.
class MongoConnection(object):
    def __init__(self):
        client = MongoClient('localhost', 27017)
        bd = client['DB']
        self.collection = bd["Pieces"]

class Pieces(MongoConnection):
    def __init__(self):
        super(Pieces, self).__init__()
        self.pieces = None
        self.pieces2 = None
        self.getDataAsDf()
        #self.pieces.to_csv("database.csv")  ## to generate .csv of query.
        #self.pieces2.to_csv("database2.csv")

    def getDataAsDf(self):
        """
        makes query in mongodb server to get and compose a dataframe to be procesed
        """
        # Query pieces perspective

        pipeline = [
            {
                "$project":
                    {
                        "_id": 1,
                        "prj_name": 1,
                        "piece_id": 1,
                        "piece_name": 1,
                        "CreationDate": 1,
                        "Status": 1,
                        "Operator": 1,
                        "controlNb": 1,
                        "measuredNb": 1,
                        "failedNb": 1,
                        "event": "$custom_properties.Event",
                        "line": "$custom_properties.Line",
                        "model": "$custom_properties.Model",
                        "NbMQSFailed": "$custom_properties.Nb MQS Failed",
                        "NbMQSPass": "$custom_properties.Nb MQS Pass",
                        "Supplier": "$custom_properties.Supplier",
                        "Unit": "$custom_properties.Unit",
                        "cp": "$spc.tolerances.cp",
                        "cpk": "$spc.tolerances.cpk",
                        "pp": "$spc.tolerances.pp",
                        "ppk": "$spc.tolerances.ppk",
                        "cg": "$spc.tolerances.cg",
                        "cgk": "$cgk.tolerances.cgk",
                        "measurementObjects": {"$size": "$measurement_objects"}
                    }
            }
        ]
        # Query controls perspective
        pipeline2 = [
            {"$unwind": "$measurement_objects"},
            {"$unwind": "$measurement_objects.measured_dimensions"},
            {
                "$project":
                    {
                        "id": 1,
                        "prj_name": 1,
                        "piece_id": 1,
                        "PieceName": 1,
                        "CreationDate": 1,
                        "Status": 1,
                        "Operator": 1,
                        "controlNb": 1,
                        "measuredNb": 1,
                        "failedNb": 1,

                        "object_id": "$measurement_objects.object_id",
                        "mo_name": "$measurement_objects.mo_name",
                        "mo_type": "$measurement_objects.mo_type",
                        "mo_measstatus": "$measurement_objects.mo_measstatus",

                        "dim_name": "$measurement_objects.measured_dimensions.dim_name",
                        "object_name": "$measurement_objects.measured_dimensions.object_name",
                        "nominal_value": "$measurement_objects.measured_dimensions.nominal_value",
                        "measured_value": "$measurement_objects.measured_dimensions.measured_value",
                        "deviation_value": "$measurement_objects.measured_dimensions.deviation_value",
                        "status": "$measurement_objects.measured_dimensions.status",
                        "control_id": "$measurement_objects.measured_dimensions.control_id",
                        "lower_tol": "$measurement_objects.measured_dimensions.lower_tol",
                        "lower_warn": "$measurement_objects.measured_dimensions.lower_warn",
                        "upper_tol": "$measurement_objects.measured_dimensions.upper_tol",
                        "upper_warn": "$measurement_objects.measured_dimensions.upper_warn",
                        "control_type": "$measurement_objects.measured_dimensions.control_type",
                        "dimension_type": "$measurement_objects.measured_dimensions.dimension_type"
                    }
            }
        ]
        pieces = self.collection.aggregate(pipeline2)
        pieces_list = Util.getCursorAsList(pieces)
        pieces = pd.DataFrame(pieces_list)
        self.pieces = pieces

        pieces2 = self.collection.aggregate(pipeline)
        pieces2_list = Util.getCursorAsList(pieces2)
        pieces2 = pd.DataFrame(pieces2_list)
        self.pieces2 = pieces2

        return pieces, pieces2

    def getAllPieces(self):
        """
        makes query mongodb to get all pieces
        :return: list of dictionaries with list of pieces.
        """

    def getDescripByProject(self, project, f_inicial, f_final):
        """
        makes a pandas query to get general info about project in specific date range
        :param project: Name of project
        :param f_inicial: Initial date
        :param f_final: Final date
        :return: list of dictionaries with features of project dataset.
        """
        # copy to preserve original data structure
        df_local = self.pieces.copy()

        # Preparing dates strings to filter
        try:
            # try to convert datetime range.
            start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
            # filtering by date range
            df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
        except:
            print("consulta sin rango de fecha")

        # filtering by project name
        df_local = df_local[df_local["prj_name"] == project]

        # generating variable summary
        result = Dm(df_local).datasetStatistics()

        return result  # lista_piezas.to_json(orient='records', default_handler=str)

    def getVariableDescr(self, project, f_inicial, f_final, variable):
        """
        makes query mongodb to get general info about project in specific date range
        :param project: Name of project
        :param f_inicial: Initial date
        :param f_final: Final date
        :param variable: variable in the database to describe
        :return: list of dictionaries with features of project dataset.
        """
        df_local = self.pieces.copy()
        # Preparing dates strings to filter
        try:
            # try to convert datetime range.
            start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
            # filtering by date range
            df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
        except:
            print("consulta sin rengo de fecha")

        # filtering by project name
        df_local = df_local[df_local["prj_name"] == project]

        # generating variable summary
        result = Dm(df_local).describeVar(variable)
        return result  # lista_piezas.to_json(orient='records', default_handler=str)

    def getProjects(self):
        """
        Makes query to get a list of total projects in database
        :return: list of projct
        """
        df_local = self.pieces.copy()
        agrupado = df_local.groupby('prj_name')[["CreationDate"]].agg(
            b_min=pd.NamedAgg(column="CreationDate", aggfunc="min"),
            c_sum=pd.NamedAgg(column="CreationDate", aggfunc="max")
        ).reset_index()
        
        agrupado["b_min"] = agrupado["b_min"].dt.strftime('%Y-%m-%d')
        agrupado["c_sum"] = agrupado["c_sum"].dt.strftime('%Y-%m-%d')

        return agrupado.astype('object').to_dict(orient='records')

    def compareQC(self, project, f_inicial, f_final, variables):
        df_local = self.pieces

        # revisión de las variables a extraer
        valid_variables = False
        columns = [None, None]
        try:
            columns = variables.split(',')
            if columns[0].strip() in df_local.columns and columns[1].strip() in df_local.columns:
                valid_variables = ~valid_variables
        except BaseException as error:
            print("Indicaron al menos una variable que no existe en el dataframe")
            print(error)

        if valid_variables:
            # filtering by date
            try:
                # try to convert datetime range.
                start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
                # filtering by date range
                df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
            except:
                print("consulta sin rango de fecha")
            # filtering by project name
            df_local = df_local[df_local["prj_name"] == project]
            # Getting only variables selected
            df_local = df_local[[columns[0], columns[1]]]
            result = df_local.astype('object').to_dict(orient='list')
        else:
            result = {"status": 'FAIL',
                      'message': 'Se solicito al menos una variable que no existe',
                      "detail": variables}
        return result

    def DispVar(self, project, f_inicial, f_final, variables):
        df_local = self.pieces

        # revisión de las variables a extraer
        valid_variables = False
        columns = [None, None]
        try:
            columns = variables.split(',')
            if columns[0].strip() in df_local.columns and columns[1].strip() in df_local.columns:
                valid_variables = ~valid_variables
        except BaseException as error:
            print("Indicaron al menos una variable que no existe en el dataframe")
            print(error)

        if valid_variables:
            # filtering by date
            try:
                # try to convert datetime range.
                start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
                # filtering by date range
                df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]

            except:
                print("consulta sin rango de fecha")
            # filtering by project name
            df_local = df_local[df_local["prj_name"] == project]
            DateRange = df_local["CreationDate"]


            # Getting only variables selected
            df_dispvar = df_local[[columns[0], columns[1]]]
            df_dispvar = df_dispvar.join(DateRange)

            result = df_dispvar.astype('object').to_dict(orient='list')
        else:
            result = {"status": 'FAIL',
                      'message': 'Se solicito al menos una variable que no existe',
                      "detail": variables}
        return result

    def getCountsByOperator(self, project, f_inicial, f_final):
        """
        makes a pandas query to get the counts of approved and rejected in specific date range
        :param project: Name of project
        :param f_inicial: Initial date
        :param f_final: Final date
        :return: list of dictionaries with counts of approved and rejected by operator.
        """
        # copy to preserve original data structure
        df_local = self.pieces.copy()

        # Preparing dates strings to filter
        try:
            # try to convert datetime range.
            start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
            # filtering by date range
            df_local = df_local[(df_local["CreationDate"] >= start) & (df_local["CreationDate"] <= end)]
        except:
            print("consulta sin rango de fecha")

        # filtering by project name
        df_local = df_local[df_local["prj_name"] == project]

        # generating required arrays
        aux = df_local.groupby("Operator")[["Status"]].agg(
            {'Status': [("Approved", lambda x: (x=='Approved').sum()),("Rejected", lambda x: (x=='Rejected').sum())]}
        )

        aux.columns = aux.columns.droplevel()

        aux['Operator'] = aux.index.values

        result = aux.astype('object').to_dict(orient='list')

        #result = {"Opetators": aux.index.values,"Approved": aux.Approved.values, "Rejected": aux.Rejected.values}

        print(result)

        return result

    """
    PATTERN DISCOVERY METHODS
    """

    def timeSerieAnalysis(self, project):
        df_local = self.pieces
        # filtering by project name
        df_local = df_local[df_local["prj_name"] == project]
        df_analysis = Dm(df_local)
        result = df_analysis.timeSeriesAnalysis(project)
        return result

    def kPrototypeClustering(self, project):
        df_local = self.pieces2
        df_local = df_local[df_local["prj_name"] == project]
        df_analysis = Dm(df_local)
        result = df_analysis.kprototypesclustering(project)
        return result

    def getTimeSeries(self, filter, value, f_inicial, f_final):
        ## agrupar el df por fecha y contar los controles fallidos/aprobados por día.
        ts = self.pieces.copy()
        # filter variable
        ts = ts[ts[filter] == value]
        # filter date range
        start, end = Util.parseRangeStrToDatetime(f_inicial, f_final)
        ts = ts[(ts["CreationDate"] >= start) & (ts["CreationDate"] <= end)]
        # create and resample dataframe to timeserie
        ts.set_index('CreationDate', inplace=True)
        ts = ts["measuredNb"]
        ts_rs = ts.resample('D').agg(lambda x: x.sum())

        return {
            "index": ts_rs.dropna().index.strftime("%d-%m-%Y").tolist(),
            "serie": ts_rs.dropna().astype('object').tolist(),
        }

    def getHeatmapD(self, project):
        df_local = self.pieces
        # filtering by project name
        df_local = df_local[df_local["prj_name"] == project]
        result = df_local.corr()
        return result