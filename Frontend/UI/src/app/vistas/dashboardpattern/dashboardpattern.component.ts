import { Component, ElementRef, Renderer2, OnInit, ViewChild, Output, EventEmitter, Inject, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/servicios/api.service';
import { Infoproject } from 'src/app/models/infoproject.interface';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from 'src/app/models/project.interface';
import { Timeseries } from 'src/app/models/timeseries.interface';
import { ClusterData } from 'src/app/models/clusterdata.interface';
import { AssociationRules } from 'src/app/models/associationrules.interface';
import { MatAccordion } from '@angular/material/expansion';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Clipboard } from '@angular/cdk/clipboard';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import dateFormat  from 'dateformat';
import { PlotlyService } from 'angular-plotly.js';
import { Platform } from '@angular/cdk/platform';
import { position } from 'html2canvas/dist/types/css/property-descriptors/position';
//import { ThisReceiver } from '@angular/compiler';
//import DomToImage from 'dom-to-image';

export interface ProjectsOp {
  projects: string;
}

const ASSRUL_DATA: AssociationRules[] = [
  { operator: "Tricia", hours: "12 - 13", status: "failed" },
  { operator: "Jaime", hours: "12 - 13", status: "failed" },
  { operator: "Angel", hours: "12 - 13", status: "failed" },
  { operator: "Gallegos", hours: "12 - 13", status: "failed" },
  { operator: "Jahir", hours: "12 - 13", status: "failed" },
  { operator: "Ramos", hours: "12 - 13", status: "failed" },
  { operator: "Josue", hours: "12 - 13", status: "failed" },
  { operator: "Lopez", hours: "12 - 13", status: "failed" },
  { operator: "Rocio", hours: "12 - 13", status: "failed" },
  { operator: "Martinez", hours: "12 - 13", status: "failed" },
  { operator: "Iñaki", hours: "12 - 13", status: "failed" },
  { operator: "Garcia", hours: "12 - 13", status: "failed" },
  { operator: "Romulo", hours: "12 - 13", status: "failed" },
  { operator: "McGinnis", hours: "12 - 13", status: "failed" },
  { operator: "Alfredo", hours: "12 - 13", status: "failed" },
  { operator: "Jimenez", hours: "12 - 13", status: "failed" },
  { operator: "Juan", hours: "12 - 13", status: "failed" },
  { operator: "Onofre", hours: "12 - 13", status: "failed" },
  { operator: "Manuel", hours: "12 - 13", status: "failed" },
  { operator: "Garcia", hours: "12 - 13", status: "failed" },
];

@Component({
  selector: 'app-dashboardpattern',
  templateUrl: './dashboardpattern.component.html',
  styleUrls: ['./dashboardpattern.component.css'],
  providers: [ApiService]
})
export class DashboardpatternComponent implements OnInit, AfterViewInit {

  @ViewChild(MatAccordion) accordion!: MatAccordion;
  @ViewChild('divRem',{ static: false }) divRem!: ElementRef;
  @ViewChild('cotainerPrnt', { static: false }) containerPrnt!: ElementRef;
  @ViewChild('grafica', { static: false }) graficaPRNT!: ElementRef;
  @ViewChild('clusterPT', { static: false }) clusterPT!: ElementRef;
  @ViewChild('clusterPTT', { static: false }) clusterPTT!: ElementRef;
  @ViewChild('clusterTbl', { static: false }) clusterTbl!: ElementRef;

  public infoProject!: Infoproject;
  private x: Array<number> = []; //<--- test varibale for histogram chart
  private i: number = 0; //<--- test varibale for histogram chart
  //all the variables below are flags to show html
  //this can be better like creating each HTML for each graph and their information
  public edited = false;
  public edited1 = false;
  public edited2 = false;
  public edited3 = false;
  public isvisibleMCard = false;
  public isvisibleWelMCard = true;
  private projects!: Array<Project>;
  public optionsPr: ProjectsOp[] = [];
  public timeSeries!: Timeseries;
  private clusterData!: ClusterData;
  public imagePath!: string;
  public startDProject!: any;
  public endDProject!: any;
  public prTitle!: any;
  public panelOpenState = false; //<--- Expasion panel
  public heatmapData:any=[];
  private location!: Location;
  private verPr:boolean = false;
  private verMd:boolean = false;
  private verStDt:boolean = false;
  private verEnDt:boolean = false;
  private isAnomalie!: string;
  private isOpenedSTools: boolean = false;
  private isClosedAll: boolean = false;

  getDataForm = new FormGroup({
    project: new FormControl(),
    models: new FormControl()
  })

  public displayedColumns!: string[];
  public dataSource: any;

  public gphLinearTrd = {
    data: [
      { y: [0], x: [''], type: '' }, //<--- LINE CHART just add all the varibles with x and y values */
    ],
    layout: {
      title: "Tendency",
      yaxis: {
        title: "Rejected parts (percentage)",
        linecolor: "#333333",
      },
      //paper_bgcolor: '#F0F0F5',
      //plot_bgcolor:'#F0F0F5',
    },
    config: {responsive: false}
  };

  public gphLinearSer = {
    data: [
      { y: [0], x: [''], type: '' }, //<--- LINE CHART just add all the varibles with x and y values */
    ],
    layout: {
      title: "Serie",
      yaxis: {
        title: "Rejected parts (percentage)",
      },
      //paper_bgcolor: '#F0F0F5',
      //plot_bgcolor:'#F0F0F5',
    },
    config: {responsive: false}
  };

  public gphLinearSea = {
    data: [
      { y: [0], x: [''], type: '' }, //<--- LINE CHART just add all the varibles with x and y values */
    ],
    layout: {
      title: "Season",
      yaxis: {
        title: "Rejected parts (percentage)",
      },
      //paper_bgcolor: '#F0F0F5',
      //plot_bgcolor:'#F0F0F5',
    },
    config: {responsive: false}
  };

  public graphHistogram = {
    data: [
      { x: [0], type: " " }, //<-- HISTOGRAM */
    ],

  };

  public graphScatter = {
    data: [
      {
        x: [''],
        y: [''],
        z: [0],
        mode: "markers",
        name: "Failed Nr",
        marker: {
          //color: "rgb(234, 153, 153)",
          color: [0],
          size: 12,
          symbol: "circle",
          line: {
            color: "rgb(204, 204, 204)",
            width: 1
          },
          opacity: 0.9
        },
        type: 'scatter3d',
        //type: 'scatter',
      }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
      {
        x: [''],
        y: [''],
        z: [''],
        mode: "markers",
        name: "centroids",
        marker: {
          color: "rgb(255, 217, 102)",
          //color: [0],
          size: 12,
          symbol: "circle",
          line: {
            color: "rgb(204, 204, 204)",
            width: 1
          },
          opacity: 0.9
        },
        //type: 'scatter3d',
        type: 'scatter3d',
      }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
    ],
    config: {responsive: false}
  };

  public scatterlayout = {
    title: "3D Cluster",
    yaxis: {
      title: "Operator",
    }
  };

  public graphHeatmap = {
    
    data: [
      { x: [''], y: [''], z: [0], type: 'heatmap' }
    ],
    layout: {title: ''},
    config: {responsive: false}
  };

  constructor(private api: ApiService,
    //public dialog: MatDialog,
    public modelD: MatDialog,
    location: Location,
    private clipboard: Clipboard,
    private activatedRoute: ActivatedRoute,
    private platform: Platform,
    public plotlyService: PlotlyService) {

    for (this.i; this.i < 500; this.i++) { //<--- test for loop for histogram chart
      this.x[this.i] = Math.random();
    }

    this.location = location; 

  };

  ngOnInit(): void {

    this.api.getProjects().subscribe(data => {
      //console.log(data);
      this.projects = data;

      this.projects.forEach(element => {
        const temp = { "projects": element.prj_name };
        //console.log(temp)
        this.optionsPr.push(temp);
      });

      //if ( localStorage.getItem('linkPReport') == null) {
        
        this.getDataForm = new FormGroup({
          project: new FormControl(this.optionsPr[0].projects),
          models: new FormControl(this.optionsMd[0].models)
          //type: new FormControl('1',Validators.required)
        })

      /*}else {

        let indexPr = 0;
        let indexMd = 0;
        this.optionsPr.forEach((element,index) => {
          if(element.projects == localStorage.getItem('prjName')){
            indexPr = index;
          }
        });

        this.optionsMd.forEach((element, index) => {
          if(element.models == localStorage.getItem('discover')){
            indexMd = index
          }
        });

        this.getDataForm = new FormGroup({
          project: new FormControl(this.optionsPr[indexPr].projects),
          models: new FormControl(this.optionsMd[indexMd].models)
        })

      }*/

    });

  };

  ngAfterViewInit(): void {

      this.activatedRoute.queryParams.subscribe(params => {

        if(params.projectName != undefined){

          this.verPr = false;
          this.verMd = false;
          this.verStDt = false;
          this.verEnDt = false;

          this.api.getProjects().subscribe(data => {
            this.projects = data;
            this.projects.forEach(element => {
              if(element.prj_name == params.projectName){
                this.verPr = true;
                return;
              }
            });

            this.projects.forEach(element => {
              if(element.b_min == params.stDate){
                this.verStDt = true;
                return;
              }
            });

            this.projects.forEach(element => {
              if(element.c_sum == params.enDate){
                this.verEnDt = true;
                return;
              }
            });
      
            this.optionsMd.forEach(element => {
              if(element.models == params.discover){
                this.verMd = true;
                return;
              }
            });

            if(this.verPr == true && this.verMd == true && this.verStDt == true && this.verEnDt == true){
    
              this.prTitle = params.projectName;
              this.startDProject = params.stDate;
              this.endDProject = params.enDate;
              this.displayReport(params.projectName, params.discover);
    
              this.api.getDataOverview(this.startDProject, this.endDProject, params.projectName, "1").subscribe(data => {
                this.infoProject = data;
              });

              // add the following code to the final version to put the default options
              // readed from the URI in the side-panel 
              /*let indexPr = 0;
              let indexMd = 0;
              this.optionsPr.forEach((element,index) => {
                if(element.projects == localStorage.getItem('prjName')){
                  indexPr = index;
                }
              });
        
              this.optionsMd.forEach((element, index) => {
                if(element.models == localStorage.getItem('discover')){
                  indexMd = index
                }
              });

              this.getDataForm = new FormGroup({
                project: new FormControl(this.optionsPr[indexPr].projects),
                models: new FormControl(this.optionsMd[indexMd].models)
              }) */
    
              //delete local storage
              /*localStorage.setItem('linkPReport', this.location.path());
              localStorage.setItem('prjName', this.prTitle);
              localStorage.setItem('discover', params.discover);
              localStorage.setItem('stDate', this.startDProject);
              localStorage.setItem('enDate', this.endDProject);*/

            }else {
              alert('Wrong data')
            }
      
          });

        }/*else{

          if(localStorage.getItem('linkPReport') != null){

            let urlConf: any = localStorage.getItem('linkPReport')
            this.location.replaceState(urlConf)
            this.prTitle = localStorage.getItem('prjName');
            this.startDProject = localStorage.getItem('stDate');
            this.endDProject = localStorage.getItem('enDate');
            let model: any = localStorage.getItem('discover')
            this.displayReport(this.prTitle, model);

            this.api.getDataOverview(this.startDProject, this.endDProject, this.prTitle, "1").subscribe(data => {
              this.infoProject = data;
            });

            this.api.getProjects().subscribe(data => {
              this.projects = data;
        
              let indexPr = 0;
              let indexMd = 0;
              this.optionsPr.forEach((element,index) => {
                if(element.projects == localStorage.getItem('prjName')){
                  indexPr = index;
                }
              });
        
              this.optionsMd.forEach((element, index) => {
                if(element.models == localStorage.getItem('discover')){
                  indexMd = index
                }
              });

              this.getDataForm = new FormGroup({
                project: new FormControl(this.optionsPr[indexPr].projects),
                models: new FormControl(this.optionsMd[indexMd].models)
              })
        
            });
          }

        }*/

      });

  }

  public optionsMd = [
    { "models": "Anomalies" }, //<--- Cluster
    { "models": "Trends" }, //<---- Time Series
    { "models": "Connection between variables" }, // <--- Association rules
    //{ "models": "Outliers" }, // <-- Outlier 
    //{ "models": "Tree" }, // <-- Decision tree
  ]

  @ViewChild('MatPaginator', { static: false })
  set paginator(value: MatPaginator) {
    this.dataSource.paginator = value;
  }

  onSubmit(form: any) {

    this.prTitle = form.project;
    this.displayReport(form.project, form.models);

    this.projects.forEach(element => {
      if (element.prj_name == form.project) {
        this.startDProject = element.b_min.toString()
        this.endDProject = element.c_sum.toString()
      }
    });

    this.api.getDataOverview(this.startDProject, this.endDProject, form.project, "1").subscribe(data => {

      this.infoProject = data;

    });

    let section = 'patterndiscovery' + encodeURI('?projectName='+form.project+'&discover='+form.models+'&stDate='+this.startDProject+'&enDate='+this.endDProject)
    this.location.replaceState('patterndiscovery')
    this.location.replaceState(section)
    /*localStorage.setItem('linkPReport', section)
    localStorage.setItem('prjName', this.prTitle)
    localStorage.setItem('discover', form.models)
    localStorage.setItem('stDate', this.startDProject)
    localStorage.setItem('enDate', this.endDProject)*/

  }


  closeWelcomM() {
    //Erase the text of the DOM
    this.divRem.nativeElement.remove();
    this.isvisibleWelMCard = false;

  }

  displayReport(project: string, model: string) {

    switch (model) {

      //clustering
      case "Anomalies": {

        //the following variable is used in the fuction savePDF
        this.isAnomalie = model;

        if (this.isvisibleWelMCard == true) {
          this.closeWelcomM()
        }
        this.edited2 = true;
        this.edited3 = false;
        this.edited1 = false;
        this.edited = false;
        this.isvisibleMCard = true;

        this.imagePath = "/assets/img/" + project;

        this.displayedColumns = ['Operator', 'Hours', 'Status'];
        this.dataSource = new MatTableDataSource<AssociationRules>(ASSRUL_DATA);

        this.api.getClusterC(project).subscribe(data => {

          this.clusterData = data;

          //cluster
          this.graphScatter = {
            data: [
              {
                x: this.clusterData.x,
                y: this.clusterData.y,
                z: this.clusterData.z,
                mode: "markers",
                name: "Failed Nr",
                marker: {
                  //color: "rgb(234, 153, 153)",
                  color: this.clusterData.clusters,
                  size: 3,
                  symbol: "circle",
                  line: {
                    color: "rgb(204, 204, 204)",
                    width: 1
                  },
                  opacity: 0.9
                },
                type: 'scatter3d',
                //type: 'scatter',
              }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
              {
                x: [this.clusterData.centroid1[0], this.clusterData.centroid2[0],
                this.clusterData.centroid3[0], this.clusterData.centroid4[0],
                ],

                y: [this.clusterData.centroid1[1], this.clusterData.centroid2[1],
                this.clusterData.centroid3[1], this.clusterData.centroid4[1],
                ],

                z: [this.clusterData.centroid1[2], this.clusterData.centroid2[2],
                this.clusterData.centroid3[2], this.clusterData.centroid4[2],
                ],

                mode: "markers",
                name: "centroids",
                marker: {
                  color: "rgb(255, 217, 102)",
                  //color: [0],
                  size: 3,
                  symbol: "circle",
                  line: {
                    color: "rgb(204, 204, 204)",
                    width: 1
                  },
                  opacity: 0.9
                },
                type: 'scatter3d',
                //type: 'scatter',
              }, //<-- SCATTER DISPERSION 3D Graph X,Y,Z
            ],
            config: {responsive: false}
          };

        });

        break;

      }

      //time series
      case "Trends": {

        //the following variable is used in the fuction savePDF
        this.isAnomalie = model;

        if (this.isvisibleWelMCard == true) {
          this.closeWelcomM()
        }
        this.edited = true;
        this.edited3 = false;
        this.edited2 = false;
        this.edited1 = false;
        this.isvisibleMCard = true;

        this.imagePath = "/assets/img/" + project;

        this.api.getTSeriesC(project).subscribe(data => {
          this.timeSeries = data;

          //trend
          this.gphLinearTrd = {
            data: [
              { y: this.timeSeries.trend, x: this.timeSeries.index, type: 'scatter' }, //<--- LINE CHART just add all the varibles with x and y values */
            ],
            layout: {
              title: "Tendency",
              yaxis: {
                title: "Rejected parts (percentage)",
                linecolor: "#333333",
              },
              //paper_bgcolor: '#F0F0F5',
              //plot_bgcolor:'#F0F0F5',
            },
            config: {responsive: false}
          };

          //Serie
          this.gphLinearSer = {
            data: [
              { y: this.timeSeries.serie, x: this.timeSeries.index, type: 'scatter' }, //<--- LINE CHART just add all the varibles with x and y values */
            ],
            layout: {
              title: "Serie",
              yaxis: {
                title: "Rejected parts (percentage)",
              },
              //paper_bgcolor: '#F0F0F5',
              //plot_bgcolor:'#F0F0F5',
            },
            config: {responsive: false}
          };

          //Season
          this.gphLinearSea = {
            data: [
              { y: this.timeSeries.seasonality, x: this.timeSeries.index, type: 'scatter' }, //<--- LINE CHART just add all the varibles with x and y values */
            ],
            layout: {
              title: "Season",
              yaxis: {
                title: "Rejected parts (percentage)",
              },
              //paper_bgcolor: '#F0F0F5',
              //plot_bgcolor:'#F0F0F5',
            },
            config: {responsive: false}
          };

        });

        break;

      }

      //Association Rules - A priori algorithm
      case "Connection between variables": {

        //the following variable is used in the fuction savePDF
        this.isAnomalie = model;

        if (this.isvisibleWelMCard == true) {
          this.closeWelcomM()
        }
        this.edited3 = true;
        this.edited2 = false;
        this.edited1 = false;
        this.edited = false;
        this.isvisibleMCard = true;

        this.imagePath = "/assets/img/" + project;

        this.displayedColumns = ['Operator', 'Hours', 'Status'];
        this.dataSource = new MatTableDataSource<AssociationRules>(ASSRUL_DATA);


        this.api.getHeatmapC(project).subscribe(data=>{
          this.heatmapData = data
          this.graphHeatmap = {
            data: [
              {
                x: this.heatmapData.index,
                y: this.heatmapData.columns,
                z: this.heatmapData.data,
                type: 'heatmap'
              }
            ],
            layout: { title: 'Heatmap test'},
            config: {responsive: false}
          };
        });


        break;

      }

      /*outlier algorithm
      case "Outliers": {

        if(this.isvisibleWelMCard == true){
          this.closeWelcomM()
        }
        this.edited3 = true;
        this.edited2 = false;
        this.edited1 = false;
        this.edited = false;
        this.isvisibleMCard = true;

        this.imagePath = "/assets/img/"+project;

        break; 

      }

      //decision tree
      case "Tree": {

        if(this.isvisibleWelMCard == true){
          this.closeWelcomM()
        }
        this.edited3 = true;
        this.edited2 = false;
        this.edited1 = false;
        this.edited = false;
        this.isvisibleMCard = true;

        this.imagePath = "/assets/img/"+project;

        break; 

      }*/

      default: {
        console.log("error");
        break;

      }

    }

  }

  histogramPlot() {

    this.graphHistogram = {
      data: [
        { x: this.x, type: "histogram" }, //<-- HISTOGRAM */
      ],

    };

  }

  closeReportWdgt() {
    this.edited = false;
    this.edited3 = false;
    this.edited2 = false;
    this.edited1 = false;
    this.isvisibleMCard = false;
  }

  isOpndTools(){
    this.isOpenedSTools = !this.isOpenedSTools;
  }

  savePDF() {

    var btnExpandElemts: HTMLElement = document.getElementById('expandAll') as HTMLElement;
    btnExpandElemts.click();

    var btnOpnTools: HTMLElement = document.getElementById('btnOpenPane') as HTMLElement;
    if(this.isOpenedSTools == false){
      btnOpnTools.click();
    }

    /*console.log('android ---> '+this.platform.ANDROID)
    console.log('IOS ---> '+this.platform.IOS)
    console.log('FIREFOX ---> '+this.platform.FIREFOX)
    console.log('blink (google?) ---> '+this.platform.BLINK)
    console.log('webkit ---> '+this.platform.WEBKIT)
    console.log('trident ---> '+ this.platform.TRIDENT)
    console.log('edge ---> '+this.platform.EDGE)
    console.log('safari ---> '+this.platform.SAFARI)*/
    
    setTimeout(() => {

      if( this.platform.SAFARI == true ){

        //can be a function
        html2canvas(this.graficaPRNT.nativeElement).then((canvas) => {
          //console.log(canvas.getContext('webgl2', { preserveDrawingBuffer:true }))
          var imgWidth = 210;
          var pageHeight = 295;
          var imgHeight = canvas.height * imgWidth / canvas.width;
          var heightLeft = imgHeight;
  
          var imgData = canvas.toDataURL('image/png');
          var doc = new jsPDF("p","mm", "letter");
          var position = 0
  
          doc.addImage(imgData, 0, position, imgWidth, imgHeight+10);
          heightLeft -= pageHeight;
  
          while (heightLeft >= 0) {
            position = heightLeft - imgHeight;
            doc.addPage();
            doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight + 10);
            heightLeft -= pageHeight;
          }
          //const imgProps= doc.getImageProperties(imgData);
          //var width = doc.internal.pageSize.getWidth();
          //var heigh = (imgProps.height * width) / imgProps.width;
          //var imgHeigth = (canvas.height * 208) / canvas.width;
          
          var now = new Date();
          var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
          doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');
  
        });
  
      }else{
  
        if(this.isAnomalie == 'Anomalies'){
          
          //console.log('impresion')
  
          html2canvas(this.clusterPT.nativeElement).then((canvas) => {
  
            var imgHeigth = (canvas.height * 208) / canvas.width;
  
            var imgData = canvas.toDataURL('image/png');
            var doc = new jsPDF("p","mm", "letter");
  
            doc.addImage(imgData, 3, 5, 208, imgHeigth);
          
  
            const graphDiv = this.plotlyService.getInstanceByDivId('graficaImprimir')
            this.plotlyService.getPlotly().then((canvaD) => {
              var imgDataD = canvaD.toImage(graphDiv, {format: 'png', width: 800, height: 600})//, {format: 'png', width: 1024, height: 800, filename: 'test' });
              //console.log(imgData)
              imgDataD.then((dataURL: any) =>{
                //console.log(dataURL);
                //const doc = new jsPDF("p", "mm", "a4");
                var position = 65 

                doc.addImage(dataURL, 10, position, 180, 135);
  
                html2canvas(this.clusterPTT.nativeElement).then((canvasT) => {
  
                  var imgHeigthT = (canvasT.height * 208) / canvasT.width;
        
                  var imgDataT = canvasT.toDataURL('image/png');
        
                  doc.addImage(imgDataT, 3, 200, 208, imgHeigthT);
        
                  var now = new Date();
                  var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
                  doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');
                  //here add the while for page break;
                  
                });
                
              });
              
            });
  
          });
  
          
          /*const graphDiv = this.plotlyService.getInstanceByDivId('graficaImprimir')
          this.plotlyService.getPlotly().then((canva) => {
            var imgData = canva.toImage(graphDiv, {format: 'png', width: 800, height: 600})//, {format: 'png', width: 1024, height: 800, filename: 'test' });
            //console.log(imgData)
            imgData.then((dataURL: any) =>{
              //console.log(dataURL)
              //const doc = new jsPDF("p", "mm", "a4");
              doc.addImage(dataURL, 0, 0, 208, 200)
              doc.save('demo.pdf')
            })
          })*/
  
        }else { //<--- can be a function
  
          html2canvas(this.graficaPRNT.nativeElement).then((canvas) => {
            //console.log(canvas.getContext('webgl2', { preserveDrawingBuffer:true }))
            var imgWidth = 210;
            var pageHeight = 295;
            var imgHeight = canvas.height * imgWidth / canvas.width;
            var heightLeft = imgHeight;
  
            var imgData = canvas.toDataURL('image/png');
            var doc = new jsPDF("p","mm", "letter");
            var position = 0
  
            doc.addImage(imgData, 0, position, imgWidth, imgHeight-10);
            heightLeft -= pageHeight;
            //heightLeft += 16;
  
            while (heightLeft >= 0) {
              position = heightLeft - imgHeight;
              doc.addPage();
              doc.addImage(imgData, 0, position, imgWidth, imgHeight + 10);
              heightLeft -= pageHeight;
              //heightLeft += 16;
            }
            //const imgProps= doc.getImageProperties(imgData);
            //var width = doc.internal.pageSize.getWidth();
            //var heigh = (imgProps.height * width) / imgProps.width;
            //var imgHeigth = (canvas.height * 208) / canvas.width;
            var now = new Date();
            var time = dateFormat(now, "dddd, mmmm dS, yyyy, h:MM:ss TT")
            doc.save(this.prTitle+' '+time+' '+this.isAnomalie+'.pdf');
          });
  
        }
  
      }
      
    }, 2000);
    
    btnOpnTools.click();

    /*DomToImage.toPng(this.graficaPRNT.nativeElement).then(function (dataURL) {
      console.log(dataURL)
      var imgData = dataURL
      const doc = new jsPDF("p", "mm", "a4");
      doc.addImage(imgData, 0, 0, 208, 100)
      doc.save('demo1.pdf')
    })*/

    //console.log(this.graficaPRNT.nativeElement)
    /*html2canvas(this.graficaPRNT.nativeElement).then((canvas) => {
      //console.log(canvas.getContext('webgl2', { preserveDrawingBuffer:true }))
      var imgData = canvas.toDataURL('image/png')
      const doc = new jsPDF("p", "mm", "a4");
      var imgHeigth = (canvas.height * 208) / canvas.width;
      doc.addImage(imgData, 0, 0, 208, imgHeigth)
      doc.save('demo.pdf')
      /*DomToImage.toJpeg(this.graficaPRNT.nativeElement).then(function (dataURL) {
        //console.log(dataURL)
        var imgData = dataURL
        const doc = new jsPDF("p", "mm", "a4");
        //var imgHeigth = (canvas.height * 208) / canvas.width;
        doc.addImage(imgData, 0, 0, 208, 100)
        doc.save('demo1.pdf')
      })*/

    //})

  }

  /*openDialog(): void {

    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '250px',
      data: this.infoProject,
    });

    /*dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });
  }*/

  openModelDialog(): void {
    const modelDRef = this.modelD.open(DialogOverviewModelDialog, {});

    /*dialogRef.afterClosed().subscribe(result => {
      //console.log('The dialog was closed');
    });*/
  }

  copyToClipboard(){
    let URLtext = 'http://192.168.50.75'+this.location.path()
    this.clipboard.copy(URLtext) 
    alert('Copied to clipboard!')
  }

  /*select(form: any) {

    this.api.getDataOverview("2018-10-01", "2018-10-10", form.project, "1").subscribe(data => {
      //console.log(data);
      this.infoProject = data;
      //console.log(this.infoProject);
    });

  }*/

}

/*@Component({
  selector: 'dialog-dashboard',
  templateUrl: './dialog-dashboard.component.html',
  styleUrls: ['./dashboardpattern.component.css']
})
export class DialogOverviewExampleDialog {

  constructor(public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Infoproject) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}*/

@Component({
  selector: 'modelD-dashboard',
  templateUrl: './modelD-dashboard.component.html',
  styleUrls: ['./dashboardpattern.component.css']
})
export class DialogOverviewModelDialog {

  constructor(public modelDRef: MatDialogRef<DialogOverviewModelDialog>) { }

  onNoClick(): void {
    this.modelDRef.close();
  }
}
